package fiuba.algo3.PopUps;

import fiuba.algo3.Controladores.EnterEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AlertBox {
    public static void display(String titulo, String alerta) {
        Stage popUp = new Stage();
        popUp.setTitle(titulo);
        popUp.initModality(Modality.APPLICATION_MODAL);
        Label mensaje = new Label(alerta);

        Button btnAceptar = new Button("Aceptar");
        btnAceptar.setOnAction(e -> popUp.close());
        btnAceptar.addEventHandler(KeyEvent.KEY_PRESSED, new EnterEvent(e -> popUp.close()));

        VBox layout = new VBox();
        layout.setSpacing(15);
        layout.getChildren().addAll(mensaje, btnAceptar);
        layout.setAlignment(Pos.CENTER);

        Scene escena = new Scene(layout, 500, 100);
        popUp.setScene(escena);
        popUp.showAndWait();
    }
}
