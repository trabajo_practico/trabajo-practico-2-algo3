package fiuba.algo3.Controladores;

import fiuba.algo3.Vistas.VistaEleccionAlgomones;
import fiuba.algo3.Vistas.VistaPelea;
import fiuba.algo3.javafx.Main;
import fiuba.algo3.modelo.Partida.Partida;
import fiuba.algo3.modelo.algomon.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.HashMap;

public class AlgomonElegidoHandler implements EventHandler<ActionEvent> {

    private int ronda;
    private String nombreAlgomon;
    private Stage stage;
    private HashMap<String,AlgoMon> mapa = new HashMap<>();

    public AlgomonElegidoHandler(Stage primaryStage, String nombre, int ronda) {
        stage = primaryStage;
        nombreAlgomon = nombre;
        this.ronda = ronda;

        mapa.put("Charmander", new Charmander());
        mapa.put("Squirtle", new Squirtle());
        mapa.put("Bulbasaur", new Bulbasaur());
        mapa.put("Chansey", new Chansey());
        mapa.put("Rattata", new Rattata());
        mapa.put("Jigglypuff", new Jigglypuff());
    }

    @Override
    public void handle(ActionEvent event) {
        Main.actualizar(stage);

        Partida partida = PartidaHandler.getPartida();
        AlgoMon algomonElegido = mapa.get(nombreAlgomon);

        partida.agregarAlgomonAJugadorActual(algomonElegido);

        partida.cambiarTurno();

        if (ronda == 6){
            stage.setScene(new Scene(new VistaPelea(stage), Main.ancho, Main.alto));
        }else{
            stage.setScene(new Scene(new VistaEleccionAlgomones(stage,ronda + 1), Main.ancho, Main.alto));
        }
    }
}
