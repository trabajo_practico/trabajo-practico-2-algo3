package fiuba.algo3.Controladores;

import fiuba.algo3.Vistas.VistaEleccionNombres;
import fiuba.algo3.javafx.Main;
import fiuba.algo3.modelo.Partida.Partida;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class PartidaHandler implements EventHandler<ActionEvent> {

    private static PartidaHandler partidaHandler = null;
    private static Partida partida;
    private static Stage stage;

    private PartidaHandler(){}

    public static PartidaHandler getPartidaHandler(Stage primaryStage) {
        if (partidaHandler == null){
            partidaHandler = new PartidaHandler();
            stage = primaryStage;
        }
        return partidaHandler;
    }

    @Override
    public void handle(ActionEvent event) {
        Main.actualizar(stage);

        PartidaHandler.getPartidaHandler(stage);
        partidaHandler.setPartida(new Partida());
        Scene scene = new Scene(new VistaEleccionNombres(stage,1), Main.ancho, Main.alto);
        stage.setScene(scene);
    }

    public static Partida getPartida(){
        return partidaHandler.partida();
    }

    private Partida partida() {
        return partida;
    }

    public void setPartida(Partida partida) {
        this.partida = partida;
    }

    public static String getNombreJugadorActual() {
        return partida.getNombreJugadorActual();
    }
}
