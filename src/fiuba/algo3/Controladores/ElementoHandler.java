package fiuba.algo3.Controladores;

import fiuba.algo3.PopUps.AlertBox;
import fiuba.algo3.Vistas.VistaPelea;
import fiuba.algo3.javafx.Main;
import fiuba.algo3.modelo.Elementos.Elemento;
import fiuba.algo3.modelo.Partida.Partida;
import fiuba.algo3.modelo.excepciones.AlgomonMuertoException;
import fiuba.algo3.modelo.excepciones.ElementoAgotadoException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

public class ElementoHandler implements EventHandler<ActionEvent> {
    private Elemento elemento;
    private Stage stage;

    public ElementoHandler(Stage stage, Elemento elemento) {
        this.stage = stage;
        this.elemento = elemento;
    }

    @Override
    public void handle(ActionEvent event) {
        Main.actualizar(stage);

        Partida partida = PartidaHandler.getPartida();
        try{
            partida.jugadorAplicarElemento(elemento);
            partida.cambiarTurno();
        }
        catch(ElementoAgotadoException e){
            AlertBox.display("Elemento agotado!", "El elemento esta agotado. realice otra accion");
        }
        catch(AlgomonMuertoException e){
            AlertBox.display("Algomon muerto!", "El algomon esta muerto. Realice otra accion");
        }

        stage.setScene(new Scene(new VistaPelea(stage), Main.ancho, Main.alto));
        stage.show();
    }
}
