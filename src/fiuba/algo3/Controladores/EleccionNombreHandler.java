package fiuba.algo3.Controladores;

import fiuba.algo3.modelo.Partida.Partida;


public class EleccionNombreHandler {

    public static void handle(String nombre) {
    	
        Partida partida = PartidaHandler.getPartida();
        partida.setNombreJugadorActual(nombre);
        partida.cambiarTurno();
    }
}
