package fiuba.algo3.Controladores;

import fiuba.algo3.PopUps.AlertBox;
import fiuba.algo3.Vistas.VistaGanador;
import fiuba.algo3.Vistas.VistaPelea;
import fiuba.algo3.javafx.Main;
import fiuba.algo3.modelo.Partida.Partida;
import fiuba.algo3.modelo.ataque.Ataque;
import fiuba.algo3.modelo.excepciones.AlgoMonNoPuedeAtacarException;
import fiuba.algo3.modelo.excepciones.AlgomonDormidoException;
import fiuba.algo3.modelo.excepciones.AlgomonMuertoException;
import fiuba.algo3.modelo.excepciones.AtaqueAgotadoException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AtaqueHandler implements EventHandler<ActionEvent> {

    private Stage stage;
    private Ataque ataque;

    public AtaqueHandler(Stage stage, Ataque ataque) {
        this.ataque = ataque;
        this.stage = stage;
    }

    @Override
    public void handle(ActionEvent event) {
        Main.actualizar(stage);

        Partida partida = PartidaHandler.getPartida();

        try{ //Intenta atacar
            partida.jugadorAtacarCon(ataque);
            intentarCambiarTurno(partida);
        }
        catch(AlgomonDormidoException e){
            AlertBox.display("AlgoMon dormido!", "El AlgoMon esta dormido y no puede atacar. Fin del turno");
            intentarCambiarTurno(partida);
        }
        catch(AtaqueAgotadoException e){
            AlertBox.display("Ataques agotados!", "El ataque esta agotado. Realice otra accion.");
        }
        catch(AlgomonMuertoException e){
            if (!(partida.termino())){
                AlertBox.display("AlgoMon muerto!", "El AlgoMon que intenta atacar ha muerto. Elija otro para continuar");
            }
        }
        catch (AlgoMonNoPuedeAtacarException e){
            AlertBox.display("AlgoMon incapaz de atacar!", "El AlgoMon es incapaz de atacar. Fin del turno.");
            intentarCambiarTurno(partida);
        }
        if (partida.termino()){
            stage.setScene(new Scene(new VistaGanador(stage, partida.getGanador().getNombreJugador()), Main.ancho, Main.alto));
        }else{
            stage.setScene(new Scene(new VistaPelea(stage), Main.ancho, Main.alto));
        }
        stage.show();
    }

    private void intentarCambiarTurno(Partida partida) {
        try{ // Intenta cambiar de turno. Solo le deja cambiar si el algomon actual esta vivo
            partida.cambiarTurno();
        }catch(AlgomonMuertoException e){
            stage.setScene(new Scene(new VistaPelea(stage), Main.ancho, Main.alto));
            stage.show();
            if (!(partida.termino())){
                AlertBox.display("AlgoMon muerto!", "Su AlgoMon ha muerto. Elija otro para continuar.");
            }
        }
    }
}
