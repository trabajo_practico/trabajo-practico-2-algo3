package fiuba.algo3.Controladores;

import fiuba.algo3.Vistas.VistaPelea;
import fiuba.algo3.javafx.Main;
import fiuba.algo3.modelo.Partida.Partida;
import fiuba.algo3.modelo.algomon.AlgoMon;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.util.List;

public class CambiarAlgomonHandler implements EventHandler<ActionEvent> {
    private Stage stage;

    public CambiarAlgomonHandler(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void handle(ActionEvent event) {
        BorderPane nuevoBorder = (BorderPane) stage.getScene().getRoot();

        Pane centerMenu = setCenterMenu();
        Pane bottomMenu = setBottomMenu();


        nuevoBorder.bottomProperty().setValue(bottomMenu);
        nuevoBorder.centerProperty().setValue(centerMenu);
        stage.show();
    }

    private Pane setBottomMenu() {
        Main.actualizar(stage);

        Button btnAtras = new Button("Atras");
        btnAtras.setOnAction(e -> {
            stage.setScene(new Scene(new VistaPelea(stage), Main.ancho, Main.alto));
            stage.show();
        });
        btnAtras.addEventHandler(KeyEvent.KEY_PRESSED, new EnterEvent(e -> {
            stage.setScene(new Scene(new VistaPelea(stage), Main.ancho, Main.alto));
            stage.show();
        }));

        HBox bottomMenu = new HBox();
        bottomMenu.getChildren().add(btnAtras);
        bottomMenu.setAlignment(Pos.CENTER);
        bottomMenu.setPadding(new Insets(10,10,10,10));
        return bottomMenu;
    }

    private Pane setCenterMenu() {
        Label algomonesDisponibles = new Label("AlgoMones disponibles:");
        Pane bloqueAlgomones = setBloqueAlgomones();

        VBox centerMenu = new VBox(15);
        centerMenu.getChildren().addAll(algomonesDisponibles,bloqueAlgomones);
        centerMenu.setAlignment(Pos.CENTER);
        return centerMenu;
    }

    private Pane setBloqueAlgomones() {
        Partida partida = PartidaHandler.getPartida();
        List<AlgoMon> algomones = partida.getAlgomonesJugadorActual();
        HBox bloqueAlgomones = new HBox(5);
        bloqueAlgomones.setAlignment(Pos.CENTER);

        for (AlgoMon algomon : algomones){
            if ((!(partida.getAlgomonActual() == algomon)) && (algomon.estaVivo())){
            	String dataAlgoMon = algomon.getNombre() + "\n" + algomon.getLife() + "/" + algomon.getVidaMaxima() + "\n" + algomon.getAbreviaturaEstado(); 
                Button btnAlgomon = new Button(dataAlgoMon);

                Image imageAlgomon = new Image(getClass().getResourceAsStream(algomon.getImagen()));
                ImageView viewAlgomon = new ImageView(imageAlgomon);
                viewAlgomon.setFitHeight(100);
                viewAlgomon.setFitWidth(100);
                btnAlgomon.setGraphic(viewAlgomon);


                btnAlgomon.setTextAlignment(TextAlignment.CENTER);
                EventHandler<ActionEvent> ev = new CambioAlgomonHandler(stage, algomon);
                btnAlgomon.setOnAction(ev);
                btnAlgomon.addEventHandler(KeyEvent.KEY_PRESSED, new EnterEvent(ev));
                bloqueAlgomones.getChildren().add(btnAlgomon);
            }
        }
        return bloqueAlgomones;
    }
}
