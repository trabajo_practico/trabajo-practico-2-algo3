package fiuba.algo3.Controladores;

import fiuba.algo3.Vistas.VistaPelea;
import fiuba.algo3.javafx.Main;
import fiuba.algo3.modelo.Partida.Partida;
import fiuba.algo3.modelo.algomon.AlgoMon;
import fiuba.algo3.modelo.ataque.Ataque;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import java.util.List;

public class AtacarHandler implements EventHandler<ActionEvent> {

    Stage stage;

    public AtacarHandler(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void handle(ActionEvent event) {
        BorderPane nuevoBorder = (BorderPane) stage.getScene().getRoot();

        Pane centerMenu = setCenterMenu();
        Pane bottomMenu = setBottomMenu();

        nuevoBorder.bottomProperty().setValue(bottomMenu);
        nuevoBorder.centerProperty().setValue(centerMenu);
        stage.show();
    }

    private Pane setBottomMenu() {
        Main.actualizar(stage);

        Button btnAtras = new Button("Atras");
        btnAtras.setOnAction(e -> {
            stage.setScene(new Scene(new VistaPelea(stage), Main.ancho, Main.alto));
            stage.show();
        });
        btnAtras.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (KeyCode.ENTER == event.getCode()) {
                    stage.setScene(new Scene(new VistaPelea(stage), Main.ancho, Main.alto));
                    stage.show();
                }
            }
        });
        HBox bottomMenu = new HBox();
        bottomMenu.getChildren().add(btnAtras);
        bottomMenu.setAlignment(Pos.CENTER);
        bottomMenu.setPadding(new Insets(10,10,10,10));
        return bottomMenu;
    }

    private Pane setCenterMenu() {
        Label ataquesDisponibles = new Label("Ataques disponibles:");
        Pane bloqueAtaques = setBloqueAtaques();

        VBox centerMenu = new VBox(15);
        centerMenu.getChildren().addAll(ataquesDisponibles,bloqueAtaques);
        centerMenu.setAlignment(Pos.CENTER);
        return centerMenu;
    }

    private Pane setBloqueAtaques() {

        Partida partida = PartidaHandler.getPartida();
        AlgoMon algomonActual = partida.getAlgomonActual();
        List<Ataque> ataques = algomonActual.ataquesDisponibles();
        HBox bloqueAtaques = new HBox(5);
        bloqueAtaques.setAlignment(Pos.CENTER);

        for (Ataque ataque : ataques){
            Button btnAtaque = new Button(ataque.getNombre() + "\n" + ataque.ataquesRestantes() + "/" + ataque.getAtaquesMaximos());
            ImageView view = new ImageView(ataque.getImagen());
            view.setFitHeight(50);
            view.setFitWidth(50);
            btnAtaque.setGraphic(view);
            btnAtaque.setTextAlignment(TextAlignment.CENTER);
            EventHandler<ActionEvent> ev = new AtaqueHandler(stage, ataque);
            btnAtaque.setOnAction(ev);
            btnAtaque.addEventHandler(KeyEvent.KEY_PRESSED, new EnterEvent(ev));
            bloqueAtaques.getChildren().add(btnAtaque);
        }

        return bloqueAtaques;
    }
}
