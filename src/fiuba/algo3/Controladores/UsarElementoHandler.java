package fiuba.algo3.Controladores;

import fiuba.algo3.Vistas.VistaPelea;
import fiuba.algo3.javafx.Main;
import fiuba.algo3.modelo.Elementos.Elemento;
import fiuba.algo3.modelo.Partida.Partida;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.util.List;

public class UsarElementoHandler implements EventHandler<ActionEvent> {
    private Stage stage;

    public UsarElementoHandler(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void handle(ActionEvent event) {
        BorderPane nuevoBorder = (BorderPane) stage.getScene().getRoot();

        Pane centerMenu = setCenterMenu();
        Pane bottomMenu = setBottomMenu();


        nuevoBorder.bottomProperty().setValue(bottomMenu);
        nuevoBorder.centerProperty().setValue(centerMenu);
        stage.show();
    }

    private Pane setBottomMenu() {
        Main.actualizar(stage);

        Button btnAtras = new Button("Atras");
        btnAtras.setOnAction(e -> {
            stage.setScene(new Scene(new VistaPelea(stage), Main.ancho, Main.alto));
            stage.show();
        });
        btnAtras.addEventHandler(KeyEvent.KEY_PRESSED, new EnterEvent(e -> {
            stage.setScene(new Scene(new VistaPelea(stage), Main.ancho, Main.alto));
            stage.show();
        }));
        HBox bottomMenu = new HBox();
        bottomMenu.getChildren().add(btnAtras);
        bottomMenu.setAlignment(Pos.CENTER);
        bottomMenu.setPadding(new Insets(10,10,10,10));
        return bottomMenu;
    }

    private Pane setCenterMenu() {
        Label elementosDisponibles = new Label("Elementos disponibles:");
        Pane bloqueElementos = setBloqueElementos();

        VBox centerMenu = new VBox(15);
        centerMenu.getChildren().addAll(elementosDisponibles,bloqueElementos);
        centerMenu.setAlignment(Pos.CENTER);
        return centerMenu;
    }

    private Pane setBloqueElementos() {
        Partida partida = PartidaHandler.getPartida();
        List<Elemento> elementos = partida.getElementosJugadorActual();
        VBox bloqueElementos = new VBox(5);
        bloqueElementos.setAlignment(Pos.CENTER);
        
        HBox primerBloque = new HBox(10);
        HBox segundoBloque = new HBox(10);
        
        primerBloque.setAlignment(Pos.CENTER);
        segundoBloque.setAlignment(Pos.CENTER);
        
        int contadorBloque = 0;
        
        for (Elemento elemento : elementos){
        	contadorBloque++;
            Button btnElemento = new Button(elemento.getNombre() + "\n"+ elemento.cantidadRestante() + "/" + elemento.getCantidadMaxima());
            ImageView view = new ImageView( elemento.getImagen() );
            view.setFitHeight(50);
            view.setFitWidth(50);
            btnElemento.setGraphic(view);

            btnElemento.setTextAlignment(TextAlignment.CENTER);
            EventHandler<ActionEvent> ev = new ElementoHandler(stage, elemento);
            btnElemento.setOnAction(ev);
            btnElemento.addEventHandler(KeyEvent.KEY_PRESSED, new EnterEvent(ev));
            
            if ( contadorBloque < 3 ) {
            	primerBloque.getChildren().add(btnElemento);
            } else {
            	segundoBloque.getChildren().add(btnElemento);
            }
        }
        bloqueElementos.getChildren().addAll(primerBloque,segundoBloque);
        
        return bloqueElementos;
    }
}
