package fiuba.algo3.Controladores;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;


public class EnterEvent implements EventHandler<KeyEvent> {
    private final EventHandler<ActionEvent> accion;

    public EnterEvent(EventHandler<ActionEvent> algomonElegidoHandler) {
        this.accion = algomonElegidoHandler;
    }

    @Override
    public void handle(KeyEvent event) {
        if (KeyCode.ENTER == event.getCode()) {
            this.accion.handle(new ActionEvent());
        }
    }
}
