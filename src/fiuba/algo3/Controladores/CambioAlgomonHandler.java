package fiuba.algo3.Controladores;

import fiuba.algo3.PopUps.AlertBox;
import fiuba.algo3.Vistas.VistaPelea;
import fiuba.algo3.javafx.Main;
import fiuba.algo3.modelo.Partida.Partida;
import fiuba.algo3.modelo.algomon.AlgoMon;
import fiuba.algo3.modelo.excepciones.AlgomonMuertoException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class CambioAlgomonHandler implements EventHandler<ActionEvent> {
    private Stage stage;
    private AlgoMon algomon;

    public CambioAlgomonHandler(Stage stage, AlgoMon algomon) {
        this.stage = stage;
        this.algomon = algomon;
    }

    @Override
    public void handle(ActionEvent event) {
        Main.actualizar(stage);

        Partida partida = PartidaHandler.getPartida();

        try{
            partida.cambiarAlgomonActualA(algomon);
            partida.cambiarTurno();
        }
        catch(AlgomonMuertoException e){
            AlertBox.display("AlgoMon muerto!", "No puede cambiar a un AlgoMon muerto. Elija uno vivo para continuar.");
        }
        stage.setScene(new Scene(new VistaPelea(stage), Main.ancho, Main.alto));
        stage.show();
    }
}
