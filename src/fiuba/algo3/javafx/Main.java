package fiuba.algo3.javafx;

import java.io.File;

import fiuba.algo3.Vistas.VistaPrincipal;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

public class Main extends Application {

    public static double ancho = 1000;
    public static double alto = 600;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("AlgoMon");

        Media sound = new Media(new File("src/fiuba/algo3/sound/theme.mp3").toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound);
        mediaPlayer.setVolume(0.1);
        mediaPlayer.play();
        
        
        Scene escena = new Scene(new VistaPrincipal(primaryStage), Main.ancho, Main.alto);
        escena.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
        primaryStage.setScene(escena);
        primaryStage.show();
    }

    public static void actualizar(Stage stage) {
        ancho = stage.getScene().getWidth();
        alto  = stage.getScene().getHeight();
    }
}
