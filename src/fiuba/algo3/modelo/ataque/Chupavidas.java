package fiuba.algo3.modelo.ataque;


import fiuba.algo3.modelo.algomon.AlgoMon;
import javafx.scene.image.Image;

public class Chupavidas extends AtaquePlanta {

    public static double PORCENTAJE_A_AUMENTAR = 0.3;
    private AlgoMon algomon;

    public Chupavidas(AlgoMon algomonAtacante) {
        nombre   = "Chupa vidas";
        potencia = 15;
        cantidadActual = 8;
        cantidadMaxima = 8;
        algomon = algomonAtacante;
        this.setImagen(img + "chupaVidas.png");
    }

    public AlgoMon algomonAtacante(){
        return algomon;
    }
}
