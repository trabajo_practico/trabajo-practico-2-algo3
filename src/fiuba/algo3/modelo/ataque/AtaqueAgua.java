package fiuba.algo3.modelo.ataque;

import static fiuba.algo3.modelo.ModificadorDePotenciaDeAtaque.*;

public abstract class AtaqueAgua extends Ataque {
    @Override
    public int potenciaContraAlgomonPlanta() {
        return (int) (potencia * ATAQUE_AGUA_CONTRA_ALGOMON_PLANTA.getMultiplicador());
    }

    @Override
    public int potenciaContraAlgomonNormal() {
        return (int) (potencia * ATAQUE_AGUA_CONTRA_ALGOMON_NORMAL.getMultiplicador());
    }

    @Override
    public int potenciaContraAlgomonAgua() {
        return (int) (potencia * ATAQUE_AGUA_CONTRA_ALGOMON_AGUA.getMultiplicador());
    }

    @Override
    public int potenciaContraAlgomonFuego() {
        return (int) (potencia * ATAQUE_AGUA_CONTRA_ALGOMON_FUEGO.getMultiplicador());
    }
}
