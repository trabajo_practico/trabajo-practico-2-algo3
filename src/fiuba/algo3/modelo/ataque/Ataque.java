package fiuba.algo3.modelo.ataque;

import fiuba.algo3.modelo.excepciones.*;
import javafx.scene.image.Image;

public abstract class Ataque {

    protected String img = "../../img/ataque/";
    protected String nombre;
    protected int potencia;
    protected int cantidadActual;
    protected int cantidadMaxima;
    protected Image imagen;

    public void utilizado() throws AtaqueAgotadoException {

        if( this.poseeAtaques() ) {
            throw new AtaqueAgotadoException();
        }
        cantidadActual--;
    }

    public int ataquesRestantes() {
        return cantidadActual;
    }
    
    public boolean poseeAtaques(){
    	return (cantidadActual == 0);
    }

    public int getPotencia() {
        return potencia;
    }

    public abstract int potenciaContraAlgomonPlanta();

    public abstract int potenciaContraAlgomonNormal();

    public abstract int potenciaContraAlgomonAgua();

    public abstract int potenciaContraAlgomonFuego();

    public void aumentarCantidad(int cantidadAAumentar) {
        if ((cantidadActual + cantidadAAumentar) > cantidadMaxima){
            cantidadActual = cantidadMaxima;
        }else{
            cantidadActual += cantidadAAumentar;
        }
    }

    public String getNombre() {
        return nombre;
    }

    public int getAtaquesMaximos() {
        return cantidadMaxima;
    }

    public Image getImagen() {
        return imagen;
    }

    public void setImagen(String unaImagen) {
        try {
            imagen = new Image(getClass().getResourceAsStream(unaImagen));
        }catch (Exception e){};
    }
}
