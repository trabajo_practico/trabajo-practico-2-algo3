package fiuba.algo3.modelo.Turno;


import fiuba.algo3.modelo.Jugador.Jugador;

import java.util.HashMap;
import java.util.Map;

public class Turno {

    private Jugador jugadorActual;
    private Map<Jugador,Jugador> mapaJugadores;

    public Turno(Jugador jugador1, Jugador jugador2){
        mapaJugadores = new HashMap<>();
        mapaJugadores.put(jugador1, jugador2);
        mapaJugadores.put(jugador2, jugador1);
        jugadorActual = jugador1;
    }

    public void cambiarTurno(){
        jugadorActual = getOponente();
    }

    public Jugador getJugadorActual(){
        return jugadorActual;
    }

    public Jugador getOponente(){
        return mapaJugadores.get(jugadorActual);
    }

}
