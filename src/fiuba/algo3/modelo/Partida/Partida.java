package fiuba.algo3.modelo.Partida;


import fiuba.algo3.modelo.Elementos.Elemento;
import fiuba.algo3.modelo.Jugador.Jugador;
import fiuba.algo3.modelo.Turno.Turno;
import fiuba.algo3.modelo.algomon.AlgoMon;
import fiuba.algo3.modelo.ataque.*;
import fiuba.algo3.modelo.excepciones.AlgomonMuertoException;

import java.util.List;

public class Partida {

    private Jugador jugador1;
    private Jugador jugador2;
    private Turno turno;
    private Jugador ganador;

    public Partida(){
        jugador1 = new Jugador();
        jugador2 = new Jugador();
        turno = new Turno(jugador1, jugador2);
    }

    public void agregarAlgomonAJugadorActual(AlgoMon algoMon) {
        turno.getJugadorActual().elegirAlgomon(algoMon);
    }

    public void jugadorAtacarCon(Ataque brasas) {
        Jugador oponente = turno.getOponente();
        turno.getJugadorActual().atacarCon(oponente.getAlgomonActual(), brasas);
    }

    public void cambiarAlgomonActualA(AlgoMon algomon) {
        turno.getJugadorActual().cambiarAlgomonA(algomon);
    }

    public void cambiarTurno() {
        if ((turno.getJugadorActual().getAlgomonActual() == null) || (turno.getJugadorActual().getAlgomonActual().estaVivo())){
            turno.cambiarTurno();
        }else{
            throw new AlgomonMuertoException();
        }
    }

    public int cantidadAlgomonesRestantes() {
        return turno.getJugadorActual().cantidadAlgomonesRestantes();
    }

    public AlgoMon getAlgomonActual() {
        return turno.getJugadorActual().getAlgomonActual();
    }

    public void jugadorAplicarElemento(Elemento elemento) {
        turno.getJugadorActual().aplicarAAlgomon(elemento);
    }

    public List<Ataque> ataquesDeJugador() {
        return turno.getJugadorActual().ataquesDisponibles();
    }

    public void setNombreJugadorActual(String nombreJugador) {
        turno.getJugadorActual().setNombreJugador(nombreJugador);
    }

    public String getNombreJugadorActual() {
        return turno.getJugadorActual().getNombreJugador();
    }

    public AlgoMon getAlgomonActualOponente() {
        return turno.getOponente().getAlgomonActual();
    }

    public String getNombreOponente() {
        return turno.getOponente().getNombreJugador();
    }

    public List<Elemento> getElementosJugadorActual() {
        return turno.getJugadorActual().elementosDisponibles();
    }

    public List<AlgoMon> getAlgomonesJugadorActual() {
        return turno.getJugadorActual().getAlgomonesDisponibles();
    }

    public Jugador getGanador() {
        return ganador;
    }

    public boolean termino() {
        if (turno.getJugadorActual().perdio()){
            ganador = turno.getOponente();
            return true;
        }else{
            if (turno.getOponente().perdio()){
                ganador = turno.getJugadorActual();
                return true;
            }
            return false;
        }
    }
}
