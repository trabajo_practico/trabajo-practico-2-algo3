package fiuba.algo3.modelo.Jugador;

import fiuba.algo3.modelo.Elementos.*;
import fiuba.algo3.modelo.algomon.AlgoMon;
import fiuba.algo3.modelo.ataque.*;
import fiuba.algo3.modelo.excepciones.AlgomonMuertoException;

import java.util.ArrayList;
import java.util.List;

public class Jugador {

    private List<AlgoMon> listaAlgomones = new ArrayList<>();
    private AlgoMon algomonActual;
    private String nombreJugador;
    private List<Elemento> listaElementos = new ArrayList<>();

    public Jugador(){
        listaElementos.add(new Pocion());
        listaElementos.add(new Restaurador());
        listaElementos.add(new SuperPocion());
        listaElementos.add(new Vitamina());
    }

    public int cantidadAlgomonesRestantes() {
        int cantidad = 0;
    	for (AlgoMon algoMon : listaAlgomones) {
			if ( !algoMon.isDead())
				cantidad++;
		}
    	return cantidad;
    }

    public void elegirAlgomon(AlgoMon algomon) {
        if (listaAlgomones.size() == 0){
            algomonActual = algomon;
        }
        listaAlgomones.add(algomon);
    }

    public void atacarCon(AlgoMon algomonRecibe, Ataque brasas) {
        algomonActual.attack(algomonRecibe, brasas);
    }

    public AlgoMon getAlgomonActual(){
        return algomonActual;
    }

    public void cambiarAlgomonA(AlgoMon bulbasaur) {
        if (bulbasaur.estaVivo()){
            algomonActual = bulbasaur;
        }else{
            throw new AlgomonMuertoException();
        }
    }

    public List<Ataque> ataquesDisponibles() {
        return algomonActual.ataquesDisponibles();
    }

	public String getNombreJugador() {
		return nombreJugador;
	}

	public void setNombreJugador(String nombreJugador) {
		this.nombreJugador = nombreJugador;
	}

    public List<Elemento> elementosDisponibles() {
        return listaElementos;
    }

    public List<AlgoMon> getAlgomonesDisponibles() {
        return listaAlgomones;
    }

    public void aplicarAAlgomon(Elemento elemento) {
        elemento.afectarAlgomon(algomonActual);
    }

    public boolean perdio() {
        for (AlgoMon algomon : listaAlgomones){
            if (algomon.estaVivo()){
                return false;
            }
        }
        return true;
    }
}
