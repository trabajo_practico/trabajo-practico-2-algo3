package fiuba.algo3.modelo.estados;


import fiuba.algo3.modelo.algomon.AlgoMon;
import fiuba.algo3.modelo.ataque.Ataque;
import fiuba.algo3.modelo.excepciones.AlgoMonNoPuedeAtacarException;
import fiuba.algo3.modelo.excepciones.ErrorCreandoEstadoCompuestoException;

import java.util.ArrayList;
import java.util.List;

public class EstadoCompuesto implements Estado {

    List<Estado> estados;

    public EstadoCompuesto(Estado unEstado, Estado otroEstado) {
        estados = new ArrayList<>();
        if( unEstado.getClass() == otroEstado.getClass() ) {
            throw  new ErrorCreandoEstadoCompuestoException();
        }
        agregar(unEstado);
        agregar(otroEstado);
    }

    public void agregar(Estado estado) {
        estados.add( estado );
    }

    public void eliminar(Estado estado) {
        estados.remove( estado);
    }

    @Override
    public void attack(AlgoMon algomonAtacante, Ataque ataque, AlgoMon algomonRecibe) {
        if( puedeAtacar( algomonAtacante ) ) {
            algomonRecibe.attackedBy( ataque );
        }else{
            throw new AlgoMonNoPuedeAtacarException();
        }
    }

    @Override
    public boolean puedeAtacar(AlgoMon algomonAtacante) {
        Boolean puedeAtacar = true;
        for ( Estado unEstado : estados ) {
            if ( ! unEstado.puedeAtacar(algomonAtacante) ) {
                puedeAtacar = false;
            }
        }
        return puedeAtacar;
    }

    @Override
    public void setEstado(AlgoMon algoMon, Estado estado) {
    }

    @Override
    public String getAbreviatura() {
        String abrevEstados = "";
        for (Estado estado : estados){
            abrevEstados = abrevEstados + estado.getAbreviatura() + "/";
        }
        abrevEstados = abrevEstados.substring(0,abrevEstados.length() - 1);
        return abrevEstados;
    }

    @Override
    public void accionDeEstado(AlgoMon algoMon) {
        for (Estado estado : estados){
            estado.accionDeEstado(algoMon);
        }
    }

}
