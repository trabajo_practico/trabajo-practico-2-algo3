package fiuba.algo3.modelo.estados;

import fiuba.algo3.modelo.algomon.AlgoMon;
import fiuba.algo3.modelo.algomon.AlgoMonAgua;
import fiuba.algo3.modelo.ataque.Ataque;

public interface Estado {

        void attack(AlgoMon algomonAtacante, Ataque ataque, AlgoMon algomonRecibe);
        boolean puedeAtacar(AlgoMon algomonAtacante);
        void setEstado(AlgoMon algoMon, Estado estado);
        String getAbreviatura();
        void accionDeEstado(AlgoMon algoMon);
}
