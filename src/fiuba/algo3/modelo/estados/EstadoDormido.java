package fiuba.algo3.modelo.estados;

import fiuba.algo3.modelo.algomon.AlgoMon;
import fiuba.algo3.modelo.ataque.Ataque;
import fiuba.algo3.modelo.excepciones.AlgomonDormidoException;
import fiuba.algo3.modelo.excepciones.EstadoNoDormido;

public class EstadoDormido implements Estado {
    private int turnosRestantes;

    public EstadoDormido(){
        turnosRestantes = 3;
    }

    @Override
    public void attack(AlgoMon algomonAtacante, Ataque ataque, AlgoMon algomonRecibe) {
        
        if (turnosRestantes == 1){
            algomonAtacante.setEstado(new EstadoNormal());
        }
        turnosRestantes --;
        if (turnosRestantes >= 0 ) {
            throw new AlgomonDormidoException();
        }
        else {
            throw new EstadoNoDormido();
        }
    }

    @Override
    public boolean puedeAtacar(AlgoMon algoMonAtacante) {
        if(turnosRestantes > 0) {
            turnosRestantes --;
            if (turnosRestantes == 0){
                algoMonAtacante.setEstado(new EstadoQuemado());
            }
            return false;
        }
        else {
            algoMonAtacante.setEstado( new EstadoQuemado() );
        }
        return true;
    }

    @Override
    public void setEstado(AlgoMon algoMon, Estado estado) {
        if ( this.getClass() != estado.getClass() ) {
            algoMon.setEstado(new EstadoCompuesto( this, estado));
        }
    }

    @Override
    public String getAbreviatura() {
        return "DOR(" + turnosRestantes + ")";
    }

    @Override
    public void accionDeEstado(AlgoMon algoMon) {
        turnosRestantes --;
    }

}
