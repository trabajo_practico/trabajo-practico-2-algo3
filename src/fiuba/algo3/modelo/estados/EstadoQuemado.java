package fiuba.algo3.modelo.estados;

import fiuba.algo3.modelo.algomon.AlgoMon;
import fiuba.algo3.modelo.ataque.Ataque;

public class EstadoQuemado implements Estado {

    @Override
    public void attack(AlgoMon algomonAtacante, Ataque ataque, AlgoMon algomonRecibe) {
        algomonRecibe.attackedBy(ataque);
        accionDeEstado(algomonAtacante);
        if (algomonAtacante.getLife() <= 0){
            algomonAtacante.morir();
        }
    }

    @Override
    public boolean puedeAtacar(AlgoMon algomonAtacante) {
        accionDeEstado(algomonAtacante);
        return true;
    }

    @Override
    public void setEstado(AlgoMon algoMon, Estado estado) {
        if (this.getClass() != estado.getClass()) {
            algoMon.setEstado( new EstadoCompuesto( this, estado));
        }
    }

    @Override
    public String getAbreviatura() {
        return "QUE";
    }

    @Override
    public void accionDeEstado(AlgoMon algoMon) {
        algoMon.quemarse();
    }


}
