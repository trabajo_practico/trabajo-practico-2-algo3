package fiuba.algo3.modelo.estados;

import fiuba.algo3.modelo.algomon.AlgoMon;
import fiuba.algo3.modelo.ataque.Ataque;
import fiuba.algo3.modelo.excepciones.AlgomonMuertoException;

public class EstadoMuerto implements Estado {
    @Override
    public void attack(AlgoMon algomonAtacante, Ataque ataque, AlgoMon algomonRecibe) {
        throw new AlgomonMuertoException();
    }

    @Override
    public boolean puedeAtacar(AlgoMon algoMon) {
        return false;
    }

    @Override
    public void setEstado(AlgoMon algoMon, Estado estado) {
        algoMon.setEstado(estado);
    }

    @Override
    public String getAbreviatura() {
        return "MUE";
    }

    @Override
    public void accionDeEstado(AlgoMon algoMon) {
    }


}
