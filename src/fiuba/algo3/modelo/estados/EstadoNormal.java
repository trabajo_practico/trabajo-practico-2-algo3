package fiuba.algo3.modelo.estados;

import fiuba.algo3.modelo.algomon.AlgoMon;
import fiuba.algo3.modelo.ataque.Ataque;

public class EstadoNormal implements Estado {
    @Override
    public void attack(AlgoMon algomonAtacante, Ataque ataque, AlgoMon algomonRecibe) {
        if(puedeAtacar( algomonAtacante ) ) {
            algomonRecibe.attackedBy(ataque);
        }
    }

    @Override
    public boolean puedeAtacar(AlgoMon algoMon) {
        return true;
    }

    @Override
    public void setEstado(AlgoMon algoMon, Estado estado) {
        algoMon.setEstado(estado);
    }

    @Override
    public String getAbreviatura() {
        return "";
    }

    @Override
    public void accionDeEstado(AlgoMon algoMon) {
    }

}
