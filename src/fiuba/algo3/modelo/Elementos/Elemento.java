package fiuba.algo3.modelo.Elementos;


import fiuba.algo3.modelo.algomon.AlgoMon;
import javafx.scene.image.Image;

public abstract class Elemento {

    protected int cantidadActual;
    protected int cantidadMaxima;
    protected String nombre;
    protected Image imagen;
    protected String img = "../../img/elementos/";

    public abstract void afectarAlgomon(AlgoMon algomonActual);

    public String getNombre() {
        return nombre;
    }

    public int cantidadRestante() {
        return cantidadActual;
    }

    public int getCantidadMaxima() {
        return cantidadMaxima;
    }

    public Image getImagen() {
        return imagen;
    }

    protected void setImagen(String unaImagen) {
        try {
            imagen = new Image(getClass().getResourceAsStream(unaImagen));
        }catch (Exception e){};
    }
}
