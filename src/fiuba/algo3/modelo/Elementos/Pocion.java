package fiuba.algo3.modelo.Elementos;


import fiuba.algo3.modelo.algomon.AlgoMon;
import fiuba.algo3.modelo.excepciones.ElementoAgotadoException;

public class Pocion extends Elemento{
    private static int VIDA_A_CURAR = 20;

    public Pocion(){
        cantidadActual = 4;
        cantidadMaxima = 4;
        nombre = "Pocion";
        this.setImagen(img + "posion.png");
    }

    @Override
    public void afectarAlgomon(AlgoMon algomonActual) {
        if (cantidadActual == 0){
            throw new ElementoAgotadoException();
        }else{
            algomonActual.curarse(VIDA_A_CURAR);
            cantidadActual--;
        }
    }
}
