package fiuba.algo3.modelo.Elementos;


import fiuba.algo3.modelo.algomon.AlgoMon;
import fiuba.algo3.modelo.excepciones.ElementoAgotadoException;

public class Vitamina extends Elemento {
    private static int CANTIDAD_A_AUMENTAR = 2;

    public Vitamina(){
        cantidadActual = 5;
        cantidadMaxima = 5;
        nombre = "Vitamina";
        this.setImagen(img + "vitaminas.png");
    }

    @Override
    public void afectarAlgomon(AlgoMon algomonActual) {
        if (cantidadActual == 0 ){
            throw new ElementoAgotadoException();
        }else {
            algomonActual.aumentarCantidadAtaques(CANTIDAD_A_AUMENTAR);
            cantidadActual--;
        }
    }
}
