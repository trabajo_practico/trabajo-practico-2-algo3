package fiuba.algo3.modelo.Elementos;


import fiuba.algo3.modelo.algomon.AlgoMon;
import fiuba.algo3.modelo.estados.EstadoNormal;
import fiuba.algo3.modelo.excepciones.ElementoAgotadoException;

public class Restaurador extends Elemento {

    public Restaurador(){
        cantidadActual = 3;
        cantidadMaxima = 3;
        nombre = "Restaurador";
        this.setImagen(img + "restaurador.png");
    }

    @Override
    public void afectarAlgomon(AlgoMon algomonActual) {
        if (cantidadActual == 0 ){
            throw new ElementoAgotadoException();
        }else{
            algomonActual.restaurarse();
            cantidadActual--;
        }
    }
}
