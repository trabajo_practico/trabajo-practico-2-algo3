package fiuba.algo3.modelo.Elementos;

import fiuba.algo3.modelo.algomon.AlgoMon;
import fiuba.algo3.modelo.excepciones.ElementoAgotadoException;

public class SuperPocion extends Elemento {
    private static int VIDA_A_CURAR = 40;

    public SuperPocion(){
        cantidadActual = 2;
        cantidadMaxima = 2;
        nombre = "Super Pocion";
        this.setImagen(img + "superPosion.png");
    }

    @Override
    public void afectarAlgomon(AlgoMon algomon) {
        if (cantidadActual == 0 ){
            throw new ElementoAgotadoException();
        }else {
            algomon.curarse(VIDA_A_CURAR);
            cantidadActual--;
        }
    }
}
