package fiuba.algo3.modelo.algomon;


import fiuba.algo3.modelo.ataque.*;

public class Charmander extends AlgoMonFuego {

    public Charmander() {
        vidaActual = 170;
        vidaMaxima = 170;

        nombre = "Charmander";

        ataques.add( new Brasas() );
        ataques.add( new Fogonazo() );
        ataques.add( new AtaqueRapido() );
        imagen = "../img/Charmander.png";
    }
    
    public String getNombre(){
    	return this.nombre;
    }

}
