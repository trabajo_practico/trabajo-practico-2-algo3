package fiuba.algo3.modelo.algomon;

import fiuba.algo3.modelo.ataque.Ataque;
import fiuba.algo3.modelo.estados.Estado;
import fiuba.algo3.modelo.estados.EstadoMuerto;
import fiuba.algo3.modelo.estados.EstadoNormal;
import fiuba.algo3.modelo.excepciones.AlgomonDormidoException;
import fiuba.algo3.modelo.excepciones.AlgomonMuertoException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class AlgoMon {

    protected int vidaMaxima;
    protected int vidaActual;
    private static double porcentajeQuemado = 0.1;
    protected String nombre;
    protected List<Ataque> ataques = new ArrayList<>();
    protected HashMap<Class, Attackhandler> attackMap;
    protected Estado estado;
    protected String imagen;

    protected abstract void initAttackMap();

    public AlgoMon() {
        this.initAttackMap();
        this.estado = new EstadoNormal();
    }

    public void attack(AlgoMon algomonRecibe, Ataque ataque){
    	estado.attack(this, ataque, algomonRecibe);
    }

    public void attackedBy(Ataque unAtaque) {
        Attackhandler handler = this.attackMap.get(unAtaque.getClass());
        handler.attackedBy( unAtaque );
        if (!(estaVivo())){
            morir();
        }
    }

    public int getLife() {
    	return vidaActual;
    }

    public void setEstado(Estado estado){
        if (this.estado.getClass() != estado.getClass()){
            this.estado = estado;
        }
    }

    public void quemarse() {
        if( (vidaActual -= (int) (vidaMaxima * porcentajeQuemado) ) <= 0) {
            morir();
        }
    }

    public void curarse(int vidaACurar){
        if (!(estaVivo())){
            throw new AlgomonMuertoException();
        }else{
            if ((vidaActual + vidaACurar) > vidaMaxima){
                vidaActual = vidaMaxima;
            }else{
                vidaActual += vidaACurar;
            }
            estado.accionDeEstado(this);
        }
    }

    public List<Ataque> ataquesDisponibles() {
        return ataques;
    }

    public void aumentarCantidadAtaques(int cantidadAAumentar) {
        if (!(estaVivo())){
            throw new AlgomonMuertoException();
        }else{
            for (Ataque ataque : ataques) {
                ataque.aumentarCantidad(cantidadAAumentar);
            }
            estado.accionDeEstado(this);
        }
    }
    
    public boolean isDead(){
    	return (vidaActual ==  0);
    }

	public abstract String getNombre();

    public int getVidaMaxima() {
        return vidaMaxima;
    }

    public String getAbreviaturaEstado() {
        return estado.getAbreviatura();
    }

    public void morir() {
        estado = new EstadoMuerto();
        vidaActual = 0;
    }

    public String getImagen() {
        return imagen;
    }

    public void restaurarse(){
        if (!(estaVivo())){
            throw new AlgomonMuertoException();
        }else{
            estado = new EstadoNormal();
        }
    }

    public boolean estaVivo() {
        if (vidaActual > 0) return true;
        return false;
    }
}

interface Attackhandler {
    void attackedBy(Ataque unAtaque);
}