package fiuba.algo3.modelo.algomon;

import fiuba.algo3.modelo.ataque.*;


public class Rattata extends AlgoMonNormal {

    public Rattata() {
        vidaActual = 170;
        vidaMaxima = 170;

        nombre = "Rattata";

        ataques.add( new Fogonazo());
        ataques.add( new Burbuja());
        ataques.add( new AtaqueRapido());
        imagen = "../img/Rattata.png";
    }

	@Override
	public String getNombre() {
		return this.nombre;
	}

}
