package fiuba.algo3.modelo.algomon;


import fiuba.algo3.modelo.ataque.*;


public class Jigglypuff extends AlgoMonNormal {

    public Jigglypuff() {
        vidaActual = 130;
        vidaMaxima = 130;

        nombre = "Jigglypuff";

        ataques.add( new Canto());
        ataques.add( new Burbuja());
        ataques.add( new AtaqueRapido());
        imagen = "../img/Jigglypuff.jpg";
    }

	@Override
	public String getNombre() {
		return this.nombre;
	}

}
