package fiuba.algo3.modelo.algomon;


import fiuba.algo3.modelo.ataque.*;
import fiuba.algo3.modelo.estados.*;

import java.util.HashMap;

public abstract class AlgoMonFuego extends AlgoMon {

    @Override
    protected void initAttackMap() {
        attackMap = new HashMap<>();
        attackMap.put( Burbuja.class, (x) -> atacadoPorBurbuja(x));
        attackMap.put( CanionDeAgua.class,(x) -> atacadoPorCanionDeAgua(x));
        attackMap.put( LatigoCepa.class, (x) -> atacadoPorLatigoCepa(x));
        attackMap.put( AtaqueRapido.class, (x) -> atacadoAtaqueRapido(x));
        attackMap.put( Fogonazo.class, (x) -> atacadoPorFogonazo(x));
        attackMap.put( Brasas.class, (x) -> atacadoPorBrasas(x));
        attackMap.put( Canto.class, (x) -> atacadoPorCanto(x));
        attackMap.put( Chupavidas.class, (x) -> atacadoPorChupavidas((Chupavidas) x));
    }
    
    private void atacadoPorChupavidas(Chupavidas ataque) {
        ataque.utilizado();
        vidaActual -= ataque.potenciaContraAlgomonFuego();
        ataque.algomonAtacante().vidaActual += ataque.potenciaContraAlgomonFuego() *  Chupavidas.PORCENTAJE_A_AUMENTAR;
    }
    
    private void atacadoPorCanto(Ataque ataque) {
        ataque.utilizado();
        vidaActual -= ataque.potenciaContraAlgomonFuego();
        estado.setEstado( this, new EstadoDormido());
    }

    private void atacadoPorBrasas(Ataque ataque) {
        ataque.utilizado();
        vidaActual -= ataque.potenciaContraAlgomonFuego();
    }
    
    private void atacadoPorFogonazo(Ataque ataque) {
        ataque.utilizado();
        vidaActual -= ataque.potenciaContraAlgomonFuego();
        estado.setEstado(this, new EstadoQuemado());
    }
    
    private void atacadoAtaqueRapido(Ataque ataque) {
    	ataque.utilizado();
        vidaActual -= ataque.potenciaContraAlgomonFuego();
    }

    private void atacadoPorLatigoCepa(Ataque ataque) {
    	ataque.utilizado();
        vidaActual -= ataque.potenciaContraAlgomonFuego();
    }

    private void atacadoPorBurbuja(Ataque ataque) {
    	ataque.utilizado();
        vidaActual -= ataque.potenciaContraAlgomonFuego();
    }

    private void atacadoPorCanionDeAgua(Ataque ataque) {
    	ataque.utilizado();
        vidaActual -= ataque.potenciaContraAlgomonFuego();
    }
}
