package fiuba.algo3.modelo.algomon;


import fiuba.algo3.modelo.ataque.*;


public class Squirtle extends AlgoMonAgua {

    public Squirtle() {
        vidaActual = 150;
        vidaMaxima = 150;

        nombre = "Squirtle";

        ataques.add( new Burbuja());
        ataques.add( new CanionDeAgua());
        ataques.add( new AtaqueRapido());
        imagen = "../img/Squirtle.png";
    }

	@Override
	public String getNombre() {
		return this.nombre;
	}

}
