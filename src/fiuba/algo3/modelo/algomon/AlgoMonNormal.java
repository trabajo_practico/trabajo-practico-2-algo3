package fiuba.algo3.modelo.algomon;


import fiuba.algo3.modelo.ataque.*;
import fiuba.algo3.modelo.estados.*;


import java.util.HashMap;



public abstract class AlgoMonNormal extends AlgoMon{

    @Override
    protected void initAttackMap() {

        attackMap   = new HashMap<>();
        attackMap.put( Brasas.class, (x) -> atacadoPorBrasas(x));
        attackMap.put( Burbuja.class, (x) -> atacadoPorBurbuja(x));
        attackMap.put( CanionDeAgua.class, (x) -> atacadoPorCanionDeAgua(x));
        attackMap.put( Canto.class, (x) -> atacadoPorCanto(x));
        attackMap.put( Chupavidas.class, (x) -> atacadoPorChupavidas((Chupavidas) x));
        attackMap.put( Fogonazo.class, (x) -> atacadoPorFogonazo(x));
        attackMap.put( LatigoCepa.class, (x) -> atacadoPorLatigoCepa(x));
        attackMap.put( AtaqueRapido.class, (x) -> atacadoAtaqueRapido(x));
    }

    private void atacadoPorFogonazo(Ataque ataque) {
        ataque.utilizado();
        vidaActual -= ataque.potenciaContraAlgomonNormal();
        estado.setEstado( this, new EstadoQuemado());
    }

    private void atacadoPorChupavidas(Chupavidas ataque) {
        ataque.utilizado();
        vidaActual -= ataque.potenciaContraAlgomonNormal();
        ataque.algomonAtacante().vidaActual += ataque.potenciaContraAlgomonNormal() * Chupavidas.PORCENTAJE_A_AUMENTAR;
    }

    private void atacadoPorCanto(Ataque ataque) {
        ataque.utilizado();
        vidaActual -= ataque.potenciaContraAlgomonNormal();
        estado.setEstado( this, new EstadoDormido());
    }

    private void atacadoAtaqueRapido(Ataque ataque) {
        ataque.utilizado();
        vidaActual -= ataque.potenciaContraAlgomonNormal();
    }

    private void atacadoPorBrasas(Ataque ataque) {
        ataque.utilizado();
        vidaActual -= ataque.potenciaContraAlgomonNormal();
    }

    private void atacadoPorLatigoCepa(Ataque ataque) {
        ataque.utilizado();
        vidaActual -= ataque.potenciaContraAlgomonNormal();
    }

    private void atacadoPorBurbuja(Ataque ataque) {
        ataque.utilizado();
        vidaActual -= ataque.potenciaContraAlgomonNormal();
    }

    private void atacadoPorCanionDeAgua(Ataque ataque) {
        ataque.utilizado();
        vidaActual -= ataque.potenciaContraAlgomonNormal();
    }
}