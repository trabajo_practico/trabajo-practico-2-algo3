package fiuba.algo3.modelo.algomon;


import fiuba.algo3.modelo.ataque.*;

public  class Chansey extends AlgoMonNormal {

    public Chansey() {
        vidaActual = 130;
        vidaMaxima = 130;

        nombre = "Chansey";

        ataques.add( new Canto());
        ataques.add( new LatigoCepa());
        ataques.add( new AtaqueRapido());
        imagen = "../img/Chansey.png";
    }

	@Override
	public String getNombre() {
		return this.nombre;
	}

}
