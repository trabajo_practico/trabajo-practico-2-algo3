package fiuba.algo3.modelo.algomon;


import fiuba.algo3.modelo.ataque.*;

import java.util.HashMap;

public class Bulbasaur extends AlgoMonPlanta {

    public Bulbasaur() {
        vidaActual = 140;
        vidaMaxima = 140;

        nombre = "Bulbasaur";

        ataques.add( new Chupavidas(this));
        ataques.add( new LatigoCepa());
        ataques.add( new AtaqueRapido());
        imagen = "../img/Bulbasaur.png";
    }

	@Override
	public String getNombre() {
		return this.nombre;
	}

}
