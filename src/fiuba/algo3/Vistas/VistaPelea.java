package fiuba.algo3.Vistas;

import fiuba.algo3.Controladores.CambiarAlgomonHandler;
import fiuba.algo3.Controladores.UsarElementoHandler;
import fiuba.algo3.Controladores.PartidaHandler;
import fiuba.algo3.Controladores.AtacarHandler;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.security.Key;


public class VistaPelea extends BorderPane {
    public VistaPelea(Stage stage) {

        Pane topMenu = setTopMenu();
        Pane centerMenu = setCenterMenu(stage);
        Pane leftMenu = setLeftMenu();
        Pane rightMenu = setRightMenu();
        Pane bottomMenu = setBottomMenu();

        this.bottomProperty().setValue(bottomMenu);
        this.topProperty().setValue(topMenu);
        this.centerProperty().set(centerMenu);
        this.leftProperty().setValue(leftMenu);
        this.rightProperty().set(rightMenu);
        
        this.getStylesheets().add(getClass().getResource("Stadium.css").toExternalForm());
        
    }

    private Pane setBottomMenu() {
        //Seteo un boton vacio para que no se mueva la pantalla cuando se agrega el boton de agregar
        Button vacio = new Button("");
        vacio.setVisible(false);
        HBox bottomMenu = new HBox();
        bottomMenu.getChildren().add(vacio);
        bottomMenu.setPadding(new Insets(10,10,10,10));
        return bottomMenu;
    }

    private Pane setRightMenu() {
        Label nombreJugador = new Label(PartidaHandler.getPartida().getNombreOponente());
        Label labelAlgomonOponente = new Label("AlgoMon actual:");

        String nombreAlgomon = PartidaHandler.getPartida().getAlgomonActualOponente().getNombre();
        Label labelNombreAlgomon = new Label(nombreAlgomon);

        int vidaActual = PartidaHandler.getPartida().getAlgomonActualOponente().getLife();
        int vidaMaxima = PartidaHandler.getPartida().getAlgomonActualOponente().getVidaMaxima();

        Label barraVida = new Label(String.valueOf(vidaActual) + "/" + String.valueOf(vidaMaxima));

        Label estado = new Label(PartidaHandler.getPartida().getAlgomonActualOponente().getAbreviaturaEstado());

        String path = PartidaHandler.getPartida().getAlgomonActualOponente().getImagen();
        Image imagen = new Image(getClass().getResourceAsStream(path));
        ImageView vistaImagen = new ImageView(imagen);
        vistaImagen.setFitHeight(100);
        vistaImagen.setFitWidth(100);

        VBox rightMenu = new VBox(8);
        rightMenu.getChildren().addAll(nombreJugador, labelAlgomonOponente, labelNombreAlgomon, vistaImagen, barraVida, estado);
        rightMenu.setAlignment(Pos.TOP_CENTER);
        return rightMenu;
    }

    private Pane setLeftMenu() {
        Label nombreJugador = new Label(PartidaHandler.getPartida().getNombreJugadorActual());
        Label labelAlgomonActual = new Label("AlgoMon actual:");

        String nombreAlgomon = PartidaHandler.getPartida().getAlgomonActual().getNombre();
        Label labelAlgomon = new Label(nombreAlgomon);

        int vidaActual = PartidaHandler.getPartida().getAlgomonActual().getLife();
        int vidaMaxima = PartidaHandler.getPartida().getAlgomonActual().getVidaMaxima();

        Label barraVida = new Label(String.valueOf(vidaActual) + "/" + String.valueOf(vidaMaxima));

        Label estado = new Label(PartidaHandler.getPartida().getAlgomonActual().getAbreviaturaEstado());

        String path = PartidaHandler.getPartida().getAlgomonActual().getImagen();
        Image imagen = new Image(getClass().getResourceAsStream(path));
        ImageView vistaImagen = new ImageView(imagen);
        vistaImagen.setFitHeight(100);
        vistaImagen.setFitWidth(100);

        VBox leftMenu = new VBox(8);
        leftMenu.getChildren().addAll(nombreJugador,labelAlgomonActual, labelAlgomon, vistaImagen, barraVida, estado);
        leftMenu.setAlignment(Pos.BOTTOM_CENTER);
        return leftMenu;
    }

    private Pane setCenterMenu(Stage stage) {
        String nombreJugador = PartidaHandler.getPartida().getNombreJugadorActual();
        Label labelTitulo = new Label(nombreJugador + " realice una accion:");

        Button btnAtacar = new Button("Atacar");
        btnAtacar.setOnAction(new AtacarHandler(stage));
        btnAtacar.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (KeyCode.ENTER == event.getCode()) {
                    (new AtacarHandler(stage)).handle(new ActionEvent());
                }
            }
        });

        Button btnElemento = new Button("Usar elemento");
        btnElemento.setOnAction(new UsarElementoHandler(stage));
        btnElemento.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (KeyCode.ENTER == event.getCode()) {
                    (new UsarElementoHandler(stage)).handle(new ActionEvent());
                }
            }
        });

        Button btnCambiar = new Button("Cambiar AlgoMon");
        btnCambiar.setOnAction(new CambiarAlgomonHandler(stage));
        btnCambiar.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (KeyCode.ENTER == event.getCode()) {
                    (new CambiarAlgomonHandler(stage)).handle(new ActionEvent());
                }
            }
        });

        HBox boxOpciones = new HBox(5);
        boxOpciones.setAlignment(Pos.CENTER);
        boxOpciones.getChildren().addAll(btnAtacar, btnElemento, btnCambiar);

        VBox centerMenu = new VBox(15);
        centerMenu.getChildren().addAll(labelTitulo, boxOpciones);
        centerMenu.setAlignment(Pos.CENTER);
        return centerMenu;
    }

    private Pane setTopMenu() {
        Label titulo = new Label("Pelea!");
        Label jugador = new Label(PartidaHandler.getPartida().getNombreJugadorActual() + " es tu turno!");
        VBox topMenu = new VBox();
        topMenu.setAlignment(Pos.CENTER);
        topMenu.getChildren().addAll(titulo, jugador);
        return topMenu;
    }
}
