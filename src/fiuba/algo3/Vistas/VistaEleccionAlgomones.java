package fiuba.algo3.Vistas;

import fiuba.algo3.Controladores.AlgomonElegidoHandler;
import fiuba.algo3.Controladores.EnterEvent;
import fiuba.algo3.Controladores.PartidaHandler;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import static javafx.scene.input.KeyEvent.KEY_PRESSED;


public class VistaEleccionAlgomones extends BorderPane {
	

    public VistaEleccionAlgomones(Stage primaryStage, int ronda) {

        Pane topMenu = setTopMenu();
        Pane centerMenu = setCenterMenu(primaryStage, ronda);
        
        this.setStyle("-fx-background-color: black");
        this.getStylesheets().add(getClass().getResource("ChoosingAlgoMon.css").toExternalForm());
        this.topProperty().setValue(topMenu);
        this.centerProperty().setValue(centerMenu);
    }

    private Pane setCenterMenu(Stage primaryStage, int ronda) {

        String nombreJugador = PartidaHandler.getNombreJugadorActual();
        Label label = new Label(nombreJugador + " elija su AlgoMon numero " + (int) Math.ceil( ronda / 2.0));

        Button btnCharmander  = buttonElegirAlgomon(primaryStage, ronda,"Charmander", "../img/Charmander.png");

        Button btnSquirtle    = buttonElegirAlgomon(primaryStage, ronda, "Squirtle", "../img/Squirtle.png");

        Button btnBulbasaur   = buttonElegirAlgomon(primaryStage, ronda, "Bulbasaur", "../img/Bulbasaur.png");

        HBox primerosAlgomones = new HBox(10);
        primerosAlgomones.getChildren().addAll(btnCharmander, btnSquirtle, btnBulbasaur);
        primerosAlgomones.setAlignment(Pos.CENTER);

        Button btnRattata    = buttonElegirAlgomon(primaryStage, ronda, "Rattata", "../img/Rattata.png");

        Button btnChansey    = buttonElegirAlgomon(primaryStage, ronda, "Chansey", "../img/Chansey.png");

        Button btnJigglypuff = buttonElegirAlgomon(primaryStage, ronda, "Jigglypuff", "../img/Jigglypuff.jpg");

        HBox segundosAlgomones = new HBox(10);
        segundosAlgomones.getChildren().addAll(btnRattata, btnChansey, btnJigglypuff);
        segundosAlgomones.setAlignment(Pos.CENTER);

        VBox centerMenu = new VBox(10);
        centerMenu.getChildren().addAll(label, primerosAlgomones, segundosAlgomones);
        centerMenu.setAlignment(Pos.CENTER);

        return centerMenu;
    }

    private Button buttonElegirAlgomon(Stage primaryStage, int ronda, String nombre, String imagen) {
        Button btn = new Button(nombre);
        EventHandler<ActionEvent> ev = new AlgomonElegidoHandler(primaryStage, btn.getText(), ronda);
        btn.setOnAction(ev);
        agregarImagenBtn(btn, imagen);
        btn.addEventHandler(KEY_PRESSED, new EnterEvent(ev));
        return btn;
    }

    private void agregarImagenBtn(Button btn, String path) {
        Image algomon = new Image(getClass().getResourceAsStream(path));
        ImageView viewAlgomon = new ImageView(algomon);
        viewAlgomon.setFitHeight(100);
        viewAlgomon.setFitWidth(100);
        btn.setGraphic(viewAlgomon);
    }

    private Pane setTopMenu() {
        Label titulo = new Label("Eleccion de algomones");
        HBox menu = new HBox();
        menu.setAlignment(Pos.CENTER);
        menu.getChildren().add(titulo);
        return menu;
    }

}
