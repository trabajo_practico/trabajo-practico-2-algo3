package fiuba.algo3.Vistas;

import fiuba.algo3.Controladores.EnterEvent;
import fiuba.algo3.Controladores.PartidaHandler;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.stage.Stage;


public class VistaPrincipal extends BorderPane {
    public VistaPrincipal(Stage primaryStage){

        Button btnIniciar;
        Button btnSalir;

        HBox topMenu = new HBox();
        Label titulo = new Label("Juego Algomon");
        topMenu.setAlignment(Pos.CENTER);
        topMenu.getChildren().add(titulo);

        HBox bottomMenu = new HBox();
        btnIniciar = new Button("Iniciar juego");
        btnIniciar.setOnAction(PartidaHandler.getPartidaHandler(primaryStage));
        btnIniciar.addEventHandler(KeyEvent.KEY_PRESSED, new EnterEvent(PartidaHandler.getPartidaHandler(primaryStage)));

        btnSalir = new Button("Salir");
        btnSalir.setOnAction(e -> primaryStage.close());
        btnSalir.addEventHandler(KeyEvent.KEY_PRESSED, new EnterEvent(e -> primaryStage.close()));

        bottomMenu.getChildren().addAll(btnIniciar, btnSalir);
        bottomMenu.setAlignment(Pos.CENTER);
        bottomMenu.setSpacing(220);
        bottomMenu.setPadding(new Insets(10,10,40,10));

        this.topProperty().setValue(topMenu);
        this.bottomProperty().setValue(bottomMenu);
    }

}
