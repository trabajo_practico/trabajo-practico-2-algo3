package fiuba.algo3.Vistas;

import fiuba.algo3.Controladores.EleccionNombreHandler;
import fiuba.algo3.Controladores.EnterEvent;
import fiuba.algo3.PopUps.AlertBox;
import fiuba.algo3.javafx.Main;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;


public class VistaEleccionNombres extends BorderPane {
    public VistaEleccionNombres(Stage primaryStage, int nroJugador) {

        Label titulo = new Label("Elección de nombre");
        HBox topMenu = new HBox();
        topMenu.setAlignment(Pos.CENTER);
        topMenu.getChildren().add(titulo);

        Label label = new Label("Jugador " + nroJugador + ": ingrese su nombre");
        TextField nombre = new TextField();
        nombre.setMaxWidth(150);
        nombre.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                Main.actualizar(primaryStage);

                if (event.getCode().equals(KeyCode.ENTER)){
                    if (nombre.getText().length() > 15){
                        AlertBox.display("Nombre invalido", "El nombre no puede superar los 15 caracteres");
                    }else{
                        EleccionNombreHandler.handle(nombre.getText());
                        if (nroJugador == 2 && nombre.getLength() > 0 ){
                            primaryStage.setScene(new Scene(new VistaEleccionAlgomones(primaryStage, 1), Main.ancho, Main.alto));
                        }else if (nombre.getLength() > 0){
                            primaryStage.setScene(new Scene(new VistaEleccionNombres(primaryStage,2), Main.ancho, Main.alto));
                        }
                    }
                }
            }
        });
        Button btnAceptar = new Button("Aceptar");
        EventHandler<ActionEvent> ev = e -> {
            Main.actualizar(primaryStage);

            if (nombre.getText().length() > 15){
                AlertBox.display("Nombre invalido", "El nombre no puede superar los 15 caracteres");
            }else{
                EleccionNombreHandler.handle(nombre.getText());
                if (nroJugador == 2 && nombre.getLength() > 0 ){
                    primaryStage.setScene(new Scene(new VistaEleccionAlgomones(primaryStage, 1), Main.ancho, Main.alto));
                }else if (nombre.getLength() > 0){
                    primaryStage.setScene(new Scene(new VistaEleccionNombres(primaryStage,2), Main.ancho, Main.alto));
                }
            }
        };
        btnAceptar.setOnAction(ev);
        btnAceptar.addEventHandler(KeyEvent.KEY_PRESSED, new EnterEvent(ev));

        VBox centerMenu = new VBox(8);
        centerMenu.getChildren().addAll(label, nombre, btnAceptar);
        centerMenu.setAlignment(Pos.TOP_CENTER);

        this.getStylesheets().add(getClass().getResource("appGame.css").toExternalForm());
        this.topProperty().setValue(topMenu);
        this.centerProperty().setValue(centerMenu);
    }
}
