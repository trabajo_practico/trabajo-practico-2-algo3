package fiuba.algo3.Vistas;

import fiuba.algo3.javafx.Main;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class VistaGanador extends BorderPane {
    public VistaGanador(Stage stage, String nombreJugador) {

        Label labelGanador = new Label("El jugador " + nombreJugador + " ha ganado!!");

        VBox centerMenu = new VBox(300);
        centerMenu.setAlignment(Pos.CENTER);

        HBox opcionesMenu = new HBox(50);
        opcionesMenu.setAlignment(Pos.CENTER);

        //imagen ganador 
        this.getStylesheets().add(getClass().getResource("victory.css").toExternalForm());
        
        Button btnJugarNuevamente = new Button("Volver a jugar!");
        btnJugarNuevamente.setOnAction(e -> {
            Scene escena = new Scene(new VistaPrincipal(stage), Main.ancho, Main.alto);
            escena.getStylesheets().add(getClass().getResource("../javafx/application.css").toExternalForm());
            stage.setScene(escena);
            stage.show();
        });

        Button btnSalir = new Button("Salir");
        btnSalir.setOnAction(e -> stage.close());

        opcionesMenu.getChildren().addAll(btnJugarNuevamente, btnSalir);
        centerMenu.getChildren().addAll(labelGanador, opcionesMenu);

        this.centerProperty().setValue(centerMenu);
    }
}
