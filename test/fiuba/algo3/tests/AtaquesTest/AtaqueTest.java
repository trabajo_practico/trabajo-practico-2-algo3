package fiuba.algo3.tests.AtaquesTest;

import fiuba.algo3.modelo.ataque.Ataque;
import fiuba.algo3.modelo.ataque.Brasas;
import fiuba.algo3.modelo.excepciones.AtaqueAgotadoException;
import org.junit.Test;

import static org.junit.Assert.*;


public class AtaqueTest {

    @Test
    public void test01CreoUnAtaqueBrasasLoUsoUnaVezYPreguntoCuantosAtaquesLeQuedan() {

        Ataque brasas = new Brasas();

        brasas.utilizado();

        assertEquals( 9, brasas.ataquesRestantes());
    }

    @Test(expected = AtaqueAgotadoException.class)
    public void test02CreoUnAtaqueBrasasYLoUsoHastaQueTermineLuegoTratoDeUsarloUnaVezMas() {

        Ataque brasas = new Brasas();

        for (int i = 0; i <  10; i++) {
            brasas.utilizado();
        }

        assertEquals( 0, brasas.ataquesRestantes());

        brasas.utilizado();
    }

    @Test
    public void test03CreoUnAtaqueBrasasYLePreguntoSuPotencia() {

        Ataque brasas   = new Brasas();

        assertEquals( 16, brasas.getPotencia());
    }
}