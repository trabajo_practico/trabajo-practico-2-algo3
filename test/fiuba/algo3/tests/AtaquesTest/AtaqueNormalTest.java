package fiuba.algo3.tests.AtaquesTest;

import static org.junit.Assert.*;

import fiuba.algo3.modelo.algomon.AlgoMon;
import fiuba.algo3.modelo.algomon.Chansey;
import fiuba.algo3.modelo.algomon.Charmander;
import fiuba.algo3.modelo.ataque.*;
import fiuba.algo3.modelo.excepciones.AlgomonDormidoException;
import fiuba.algo3.modelo.excepciones.EstadoNoDormido;
import org.junit.Test;

public class AtaqueNormalTest {

    @Test
    public void test01UnAlgomonDormidoEsAtacadoPorCantoPeroLosTurnosDormidoNoSeReinician(){
        AlgoMon charmander = new Charmander();
        AlgoMon chansey = new Chansey();

        charmander.attackedBy(new Canto());

        try{
            charmander.attack(chansey, new Fogonazo());
        }catch(AlgomonDormidoException e){}

        charmander.attackedBy(new Canto());

        for (int i = 0 ; i < 3; i++){
            try{
                charmander.attack(chansey, new Fogonazo());
            }catch(AlgomonDormidoException e){}
        }

        assertEquals(128, chansey.getLife());
    }

    @Test
    public void test02UnAlgomonDormidoYQuemadoEsAtacadoPorCantoPeroLosTurnosDormidoNoSeReinician(){
        AlgoMon charmander = new Charmander();
        AlgoMon chansey = new Chansey();

        charmander.attackedBy(new Canto());
        charmander.attackedBy(new Brasas());

        try{
            charmander.attack(chansey, new Fogonazo());
        }catch(AlgomonDormidoException e){}

        charmander.attackedBy(new Canto());

        for (int i = 0 ; i < 3; i++){
            try{
                charmander.attack(chansey, new Fogonazo());
            }catch(AlgomonDormidoException e){}
        }

        assertEquals(128, chansey.getLife());
    }
}
