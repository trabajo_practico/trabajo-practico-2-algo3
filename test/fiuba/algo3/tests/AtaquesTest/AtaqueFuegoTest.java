package fiuba.algo3.tests.AtaquesTest;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.modelo.algomon.*;
import fiuba.algo3.modelo.ataque.*;

public class AtaqueFuegoTest {

	@Test
    public void test01AtacoConBrasasAUnAlgomonTipoAguaYDebeAfectarALaMitad() {
		AlgoMon charmander 	= new Charmander();
		AlgoMon squirtle 	= new Squirtle();

		Ataque brasas = new Brasas();
        
		charmander.attack(squirtle, brasas);

        assertEquals( 142 ,squirtle.getLife());
    }
	
	@Test
    public void test02AtacoConBrasasAUnAlgomonTipoNormalYDebeAfectarElTotal() {
		AlgoMon charmander 	= new Charmander();
		AlgoMon rattata 	= new Rattata();
        
		Ataque brasas = new Brasas();
        
		charmander.attack(rattata, brasas);

        assertEquals( 154 ,rattata.getLife());
    }
	
	@Test
    public void test03AtacoConBrasasAUnAlgomonTipoPlantaYDebeAfectarElDoble() {
		AlgoMon charmander 	= new Charmander();
		AlgoMon bulbasaur	= new Bulbasaur();
        
		Ataque brasas = new Brasas();
        
		charmander.attack(bulbasaur, brasas);

        assertEquals( 108 ,bulbasaur.getLife());
    }
	
	@Test
    public void test04AtacoConFogonazoAUnAlgomonTipoAguaYDebeAfectarALaMitad() {
		AlgoMon charmander 	= new Charmander();
		AlgoMon squirtle 	= new Squirtle();
        
		//brasas quita 2 pts de vida
		Ataque fogonazo = new Fogonazo();
        
		charmander.attack(squirtle, fogonazo);

        assertEquals( 149 ,squirtle.getLife());
    }
	
	@Test
    public void test05AtacoConBrasasAUnAlgomonTipoNormalYDebeAfectarElTotal() {
		AlgoMon charmander 	= new Charmander();
		AlgoMon rattata 	= new Rattata();
        
		Ataque fogonazo = new Fogonazo();
        
		charmander.attack(rattata, fogonazo);

        assertEquals( 168 ,rattata.getLife());
    }
	
	@Test
    public void test06AtacoConBrasasAUnAlgomonTipoPlantaYDebeAfectarElDoble() {
		AlgoMon charmander 	= new Charmander();
		AlgoMon bulbasaur	= new Bulbasaur();
        
		Ataque fogonazo = new Fogonazo();
        
		charmander.attack(bulbasaur, fogonazo);

        assertEquals( 136 ,bulbasaur.getLife());
    }

}
