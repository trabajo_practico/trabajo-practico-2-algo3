package fiuba.algo3.tests;

import fiuba.algo3.modelo.algomon.*;
import fiuba.algo3.modelo.ataque.*;
import fiuba.algo3.modelo.excepciones.*;

import org.junit.Test;

import static org.junit.Assert.*;

public class AlgoMonTest {

    // Punto 1
    @Test
    public void test01SquirtleAtacaConBurbujaACharmanderYLeQuita20puntosDeVida(){

        AlgoMon squirtle    = new Squirtle();
        AlgoMon charmander  = new Charmander();

        Ataque burbuja     = new Burbuja();

        squirtle.attack( charmander, burbuja );

        assertEquals( 150, charmander.getLife());

    }

    @Test
    public void test02SquirtleAtacaConCanionDeAguaACharmanderYLeQuita40puntosDeVida(){

        AlgoMon squirtle    = new Squirtle();
        AlgoMon charmander  = new Charmander();

        Ataque canionDeAgua = new CanionDeAgua();

        squirtle.attack( charmander, canionDeAgua );

        assertEquals( 130 , charmander.getLife());
    }

    // Punto 2
    @Test
    public void test03SquirtleAtacaConBurbujaABulbasaurYLeQuita5puntosDeVida(){

        AlgoMon squirtle    = new Squirtle();
        AlgoMon bulbasaur  = new Bulbasaur();

        Ataque burbuja     = new Burbuja();

        squirtle.attack( bulbasaur, burbuja );

        assertEquals( 135, bulbasaur.getLife());
    }

    @Test
    public void test04SquirtleAtacaConCanionDeAguaABulbasaurYLeQuita10puntosDeVida(){

        AlgoMon squirtle    = new Squirtle();
        AlgoMon bulbasaur  = new Bulbasaur();

        Ataque canionDeAgua = new CanionDeAgua();

        squirtle.attack( bulbasaur ,canionDeAgua);

        assertEquals( 130 , bulbasaur.getLife());
    }

    // Punto 3
    @Test
    public void test05SquirtleAtacaConBurbujaARattataYLeQuita10puntosDeVida(){
        AlgoMon squirtle    = new Squirtle();
        AlgoMon rattata     = new Rattata();

        Ataque burbuja      = new Burbuja();

        squirtle.attack( rattata, burbuja );

        assertEquals( 160, rattata.getLife());
    }

    @Test
    public void test06SquirtleAtacaConCanionDeAguaARattataYLeQuita20puntosDeVida(){

        AlgoMon squirtle    = new Squirtle();
        AlgoMon rattata     = new Rattata();

        Ataque canionDeAgua = new CanionDeAgua();

        squirtle.attack( rattata, canionDeAgua);

        assertEquals( 150 , rattata.getLife());
    }

    @Test
    public void test07SquirtleAtacaConBurbujaAChanseyYLeQuita10puntosDeVida(){
        AlgoMon squirtle    = new Squirtle();
        AlgoMon chansey     = new Chansey();

        Ataque burbuja      = new Burbuja();

        squirtle.attack( chansey, burbuja );

        assertEquals( 120, chansey.getLife());
    }

    @Test
    public void test08SquirtleAtacaConCanionDeAguaAChanseyYLeQuita20puntosDeVida(){

        AlgoMon squirtle    = new Squirtle();
        AlgoMon chansey     = new Chansey();

        Ataque canionDeAgua = new CanionDeAgua();

        squirtle.attack( chansey, canionDeAgua );

        assertEquals( 110 , chansey.getLife());
    }

    @Test
    public void test09SquirtleAtacaConBurbujaAJigglypuffYLeQuita10puntosDeVida(){
        AlgoMon squirtle    = new Squirtle();
        AlgoMon jigglypuff  = new Jigglypuff();

        Ataque burbuja      = new Burbuja();

        squirtle.attack( jigglypuff, burbuja );

        assertEquals( 120, jigglypuff.getLife());
    }

    @Test
    public void test10SquirtleAtacaConCanionDeAguaAJigglypuffYLeQuita20puntosDeVida(){

        AlgoMon squirtle    = new Squirtle();
        AlgoMon jigglypuff  = new Jigglypuff();

        Ataque canionDeAgua = new CanionDeAgua();

        squirtle.attack( jigglypuff, canionDeAgua );

        assertEquals( 110 , jigglypuff.getLife());
    }

    // Punto 4
    @Test
    public void test11BulbasaurAtacaConLatigoCepaASquirtleYLeQuita30puntosDeVida() {

        AlgoMon bulbasaur   = new Bulbasaur();
        AlgoMon squirtle    = new Squirtle();

        Ataque latigoCepa   = new LatigoCepa();

        bulbasaur.attack( squirtle , latigoCepa);

        assertEquals( 120, squirtle.getLife());
    }

    @Test
    public void test12ChanseyAtacaConLatigoCepaASquirtleYLeQuita30puntosDeVida() {
        AlgoMon chansey     = new Chansey();
        AlgoMon squirtle    = new Squirtle();

        Ataque latigoCepa   = new LatigoCepa();

        chansey.attack( squirtle, latigoCepa );

        assertEquals( 120, squirtle.getLife());
    }

    // Punto 5
    @Test
    public void test13BulbasaurAtacaConLatigoCepaACharmanderYLeQuita7puntosDeVida() {
        AlgoMon bulbasaur    = new Bulbasaur();
        AlgoMon charmander  = new Charmander();

        Ataque latigoCepa   = new LatigoCepa();

        bulbasaur.attack( charmander, latigoCepa );

        assertEquals( 163, charmander.getLife());
    }

    @Test
    public void test14ChanseyAtacaConLatigoCepaACharmanderYLeQuita7puntosDeVida() {
        AlgoMon chansey     = new Chansey();
        AlgoMon charmander  = new Charmander();

        Ataque latigoCepa   = new LatigoCepa();

        chansey.attack( charmander, latigoCepa );

        assertEquals( 163, charmander.getLife());
    }

    // Punto 6
    @Test
    public void test15BulbasaurAtacaConLatigoCepaAJigglypuffYLeQuita15puntosDeVida() {
        AlgoMon bulbasaur   = new Bulbasaur();
        AlgoMon jigglypuff  = new Jigglypuff();

        Ataque latigoCepa   = new LatigoCepa();

        bulbasaur.attack( jigglypuff, latigoCepa );

        assertEquals( 115, jigglypuff.getLife());
    }

    @Test
    public void test16BulbasaurAtacaConLatigoCepaAChanseyYLeQuita15puntosDeVida() {
        AlgoMon bulbasaur   = new Bulbasaur();
        AlgoMon chansey     = new Chansey();

        Ataque latigoCepa   = new LatigoCepa();

        bulbasaur.attack( chansey , latigoCepa);

        assertEquals( 115, chansey.getLife());
    }

    @Test
    public void test17BulbasaurAtacaConLatigoCepaARattataYLeQuita15puntosDeVida() {
        AlgoMon bulbasaur   = new Bulbasaur();
        AlgoMon rattata     = new Rattata();

        Ataque latigoCepa   = new LatigoCepa();

        bulbasaur.attack( rattata ,latigoCepa);

        assertEquals( 155, rattata.getLife());
    }

    @Test
    public void test18ChanseyAtacaConLatigoCepaAJigglypuffYLeQuita15puntosDeVida() {
        AlgoMon chansey   = new Chansey();
        AlgoMon jigglypuff  = new Jigglypuff();

        Ataque latigoCepa   = new LatigoCepa();

        chansey.attack( jigglypuff , latigoCepa);

        assertEquals( 115, jigglypuff.getLife());
    }

    @Test
    public void test19ChanseyAtacaConLatigoCepaAChanseyYLeQuita15puntosDeVida() {
        AlgoMon chanseyAtacante   = new Chansey();
        AlgoMon chansey     = new Chansey();

        Ataque latigoCepa   = new LatigoCepa();

        chanseyAtacante.attack( chansey , latigoCepa);

        assertEquals( 115, chansey.getLife());
    }

    @Test
    public void test20ChanseyAtacaConLatigoCepaARattataYLeQuita15puntosDeVida() {
        AlgoMon chansey   = new Chansey();
        AlgoMon rattata     = new Rattata();

        Ataque latigoCepa   = new LatigoCepa();

        chansey.attack( rattata , latigoCepa);

        assertEquals( 155, rattata.getLife());
    }

    // Punto 7
    @Test
    public void test21CharmanderAtacaConBrasasABulbasaurYLeQuita32puntosDeVida() {
        AlgoMon charmander  = new Charmander();
        AlgoMon bulbasaur   = new Bulbasaur();

        Ataque brasas       = new Brasas();

        charmander.attack( bulbasaur , brasas);

        assertEquals( 108, bulbasaur.getLife());
    }

    // Punto 8
    @Test
    public void test22CharmanderAtacaConBrasasASquirtleYLeQuita8puntosDeVida() {
        AlgoMon charmander  = new Charmander();
        AlgoMon squirtle    = new Squirtle();

        Ataque brasas       = new Brasas();

        charmander.attack( squirtle , brasas);

        assertEquals( 142, squirtle.getLife());
    }

    // Punto 9
    @Test
    public void test23CharmanderAtacaConBrasasAJigglypuffYLeQuita16puntosDeVida() {
    	AlgoMon charmander 	= new Charmander();
    	AlgoMon jigglypuff 	= new Jigglypuff();
    	
    	Ataque brasas		= new Brasas();

    	charmander.attack(jigglypuff, brasas);
    	
    	assertEquals(114, jigglypuff.getLife());
    }
    
    @Test
    public void test24CharmanderAtacaConBrasasAChanseyYLeQuita16puntosDeVida() {
    	AlgoMon charmander 	= new Charmander();
    	AlgoMon chansey 	= new Chansey();
    	
    	Ataque brasas		= new Brasas();

    	charmander.attack(chansey, brasas);
    	
    	assertEquals(114, chansey.getLife());
    }
    
    @Test
    public void test25CharmanderAtacaConBrasasARattataYLeQuita16puntosDeVida() {
    	AlgoMon charmander 	= new Charmander();
    	AlgoMon rattata 	= new Rattata();
    	
    	Ataque brasas		= new Brasas();

    	charmander.attack(rattata,brasas);
    	
    	assertEquals(154, rattata.getLife());
    }
    
    //Punto 10
    @Test
    public void test26ChanseyAtacaConAtaqueRapidoABulbasaurYLeQuita10puntosDeVida() {
    	AlgoMon chansey 	= new Chansey();
    	AlgoMon bulbasaur   = new Bulbasaur();
    	
    	Ataque ataqueRapido	= new AtaqueRapido();

    	chansey.attack(bulbasaur, ataqueRapido);
    	
    	assertEquals(130, bulbasaur.getLife());
    }
    
    @Test
    public void test27ChanseyAtacaConAtaqueRapidoACharmanderYLeQuita10puntosDeVida() {
    	AlgoMon chansey 	= new Chansey();
    	AlgoMon charmander   = new Charmander();
    	
    	Ataque ataqueRapido	= new AtaqueRapido();

    	chansey.attack(charmander, ataqueRapido);
    	
    	assertEquals(160, charmander.getLife());
    }
    
    @Test
    public void test28ChanseyAtacaConAtaqueRapidoAJigglypuffYLeQuita10puntosDeVida() {
    	AlgoMon chansey 	= new Chansey();
    	AlgoMon jigglypuff   = new Jigglypuff();
    	
    	Ataque ataqueRapido	= new AtaqueRapido();

    	chansey.attack(jigglypuff, ataqueRapido);
    	
    	assertEquals(120, jigglypuff.getLife());
    }
    
    @Test
    public void test29ChanseyAtacaConAtaqueRapidoARattataYLeQuita10puntosDeVida() {
    	AlgoMon chansey 	= new Chansey();
    	AlgoMon rattata   = new Rattata();
    	
    	Ataque ataqueRapido	= new AtaqueRapido();

    	chansey.attack(rattata, ataqueRapido);
    	
    	assertEquals(160, rattata.getLife());
    }
    
    @Test
    public void test30ChanseyAtacaConAtaqueRapidoASquirtleYLeQuita10puntosDeVida() {
    	AlgoMon chansey 	= new Chansey();
    	AlgoMon squirtle   	= new Squirtle();
    	
    	Ataque ataqueRapido	= new AtaqueRapido();

    	chansey.attack(squirtle, ataqueRapido);
    	
    	assertEquals(140, squirtle.getLife());
    }
    
    @Test
    public void test31JigglypuffAtacaConAtaqueRapidoABulbasaurYLeQuita10puntosDeVida() {
    	AlgoMon jigglypuff 		= new Jigglypuff();
    	AlgoMon bulbasaur   	= new Bulbasaur();
    	
    	Ataque ataqueRapido	= new AtaqueRapido();

    	jigglypuff.attack(bulbasaur, ataqueRapido);
    	
    	assertEquals(130, bulbasaur.getLife());
    }
    
    @Test
    public void test32JigglypuffAtacaConAtaqueRapidoACharmanderYLeQuita10puntosDeVida() {
    	AlgoMon jigglypuff 	= new Jigglypuff();
    	AlgoMon charmander   = new Charmander();
    	
    	Ataque ataqueRapido	= new AtaqueRapido();

    	jigglypuff.attack(charmander, ataqueRapido);
    	
    	assertEquals(160, charmander.getLife());
    }
    
    @Test
    public void test33JigglypuffAtacaConAtaqueRapidoAChanseyYLeQuita10puntosDeVida() {
    	AlgoMon jigglypuff 	= new Jigglypuff();
    	AlgoMon chansey   	= new Chansey();
    	
    	Ataque ataqueRapido	= new AtaqueRapido();

    	jigglypuff.attack(chansey, ataqueRapido);
    	
    	assertEquals(120, chansey.getLife());
    }
    
    @Test
    public void test34JigglypuffAtacaConAtaqueRapidoARattataYLeQuita10puntosDeVida() {
    	AlgoMon jigglypuff 	= new Jigglypuff();
    	AlgoMon rattata   	= new Rattata();
    	
    	Ataque ataqueRapido	= new AtaqueRapido();

    	jigglypuff.attack(rattata, ataqueRapido);
    	
    	assertEquals(160, rattata.getLife());
    }
    
    @Test
    public void test35JigglypuffAtacaConAtaqueRapidoASquirtleYLeQuita10puntosDeVida() {
    	AlgoMon jigglypuff	= new Jigglypuff();
    	AlgoMon squirtle   	= new Squirtle();
    	
    	Ataque ataqueRapido	= new AtaqueRapido();

    	jigglypuff.attack(squirtle, ataqueRapido);
    	
    	assertEquals(140, squirtle.getLife());
    }
    
    @Test
    public void test36RattataAtacaConAtaqueRapidoABulbasaurYLeQuita10puntosDeVida() {
    	AlgoMon rattata 		= new Rattata();
    	AlgoMon bulbasaur   	= new Bulbasaur();
    	
    	Ataque ataqueRapido	= new AtaqueRapido();

    	rattata.attack(bulbasaur, ataqueRapido);
    	
    	assertEquals(130, bulbasaur.getLife());
    }
    
    @Test
    public void test37RattataAtacaConAtaqueRapidoACharmanderYLeQuita10puntosDeVida() {
    	AlgoMon rattata 	= new Rattata();
    	AlgoMon charmander 	= new Charmander();
    	
    	Ataque ataqueRapido	= new AtaqueRapido();

    	rattata.attack(charmander, ataqueRapido);
    	
    	assertEquals(160, charmander.getLife());
    }
    
    @Test
    public void test38RattataAtacaConAtaqueRapidoAChanseyYLeQuita10puntosDeVida() {
    	AlgoMon rattata 	= new Rattata();
    	AlgoMon chansey   	= new Chansey();
    	
    	Ataque ataqueRapido	= new AtaqueRapido();

    	rattata.attack(chansey, ataqueRapido);
    	
    	assertEquals(120, chansey.getLife());
    }
    
    @Test
    public void test39RattataAtacaConAtaqueRapidoAJigglypuffYLeQuita10puntosDeVida() {
    	AlgoMon rattata 		= new Rattata();
    	AlgoMon jigglypuff   	= new Jigglypuff();
    	
    	Ataque ataqueRapido	= new AtaqueRapido();

    	rattata.attack(jigglypuff, ataqueRapido);
    	
    	assertEquals(120, jigglypuff.getLife());
    }
    
    @Test
    public void test40RattataAtacaConAtaqueRapidoASquirtleYLeQuita10puntosDeVida() {
    	AlgoMon rattata 	= new Rattata();
    	AlgoMon squirtle   	= new Squirtle();
    	
    	Ataque ataqueRapido	= new AtaqueRapido();

    	rattata.attack(squirtle,ataqueRapido);
    	
    	assertEquals(140, squirtle.getLife());
    }

    // Punto 11
    @Test(expected= AtaqueAgotadoException.class)
    public void test41CharmanderAtacaConFogonazoABulbasaurHastaQuedarseSinMasAtaques() {
        AlgoMon charmander  = new Charmander();
        AlgoMon bulbasaur   = new Bulbasaur();

        Ataque fogonazo     = new Fogonazo();

        Boolean NoAttack = fogonazo.poseeAtaques();
        
        while (! NoAttack ) {
        	charmander.attack( bulbasaur , fogonazo);
        	NoAttack = fogonazo.poseeAtaques();
        }
        
        assertEquals( 124, bulbasaur.getLife());
        
        charmander.attack( bulbasaur, fogonazo );
    }

    @Test(expected= AtaqueAgotadoException.class)
    public void test42CharmanderAtacaConFogonazoAChanseyHastaQuedarseSinMasAtaques() {
        AlgoMon charmander  = new Charmander();
        AlgoMon chansey   = new Chansey();

        Ataque fogonazo     = new Fogonazo();

        Boolean NoAttack = fogonazo.poseeAtaques();
        
        while (! NoAttack ) {
        	charmander.attack( chansey, fogonazo );
        	NoAttack = fogonazo.poseeAtaques();
        }
        
        assertEquals( 122, chansey.getLife());
        
        charmander.attack( chansey, fogonazo );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test43CharmanderAtacaConFogonazoAJigglypuffHastaQuedarseSinMasAtaques() {
        AlgoMon charmander  = new Charmander();
        AlgoMon jigglypuff  = new Jigglypuff();

        Ataque fogonazo     = new Fogonazo();

        Boolean NoAttack = fogonazo.poseeAtaques();
        
        while (! NoAttack ) {
        	charmander.attack( jigglypuff, fogonazo);
        	NoAttack = fogonazo.poseeAtaques();
        }
        
        assertEquals( 122, jigglypuff.getLife());
        
        charmander.attack( jigglypuff, fogonazo );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test44CharmanderAtacaConFogonazoARattataHastaQuedarseSinMasAtaques() {
        AlgoMon charmander  = new Charmander();
        AlgoMon rattata   = new Rattata();

        Ataque fogonazo     = new Fogonazo();

        Boolean NoAttack = fogonazo.poseeAtaques();
        
        while (! NoAttack ) {
        	charmander.attack( rattata , fogonazo);
        	NoAttack = fogonazo.poseeAtaques();
        }
        
        assertEquals( 162, rattata.getLife());
        
        charmander.attack( rattata, fogonazo );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test45CharmanderAtacaConFogonazoASquirtleHastaQuedarseSinMasAtaques() {
        AlgoMon charmander  = new Charmander();
        AlgoMon squirtle   	= new Squirtle();

        Ataque fogonazo     = new Fogonazo();

        Boolean NoAttack = fogonazo.poseeAtaques();
        
        while (! NoAttack ) {
        	charmander.attack( squirtle, fogonazo );
        	NoAttack = fogonazo.poseeAtaques();
        }
        
        assertEquals( 146, squirtle.getLife());
        
        charmander.attack( squirtle, fogonazo );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test46CharmanderAtacaConFogonazoACharmanderHastaQuedarseSinMasAtaques() {
        AlgoMon charmander  	= new Charmander();
        AlgoMon otroCharmander	= new Charmander();

        Ataque fogonazo     = new Fogonazo();

        Boolean NoAttack = fogonazo.poseeAtaques();
        
        while (! NoAttack ) {
        	charmander.attack( otroCharmander , fogonazo);
        	NoAttack = fogonazo.poseeAtaques();
        }
        
        assertEquals( 166, otroCharmander.getLife());
        
        charmander.attack( otroCharmander, fogonazo );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test47CharmanderAtacaConBrasasABulbasuarHastaQuedarseSinMasAtaques() {
        AlgoMon charmander  	= new Charmander();
        AlgoMon bulbasaur		= new Bulbasaur();

        Ataque brasas     = new Brasas();

        Boolean NoAttack = brasas.poseeAtaques();
        
        while (! NoAttack ) {
        	charmander.attack( bulbasaur , brasas);
        	NoAttack = brasas.poseeAtaques();
        }
        
        assertEquals( 0, bulbasaur.getLife());
        
        charmander.attack( bulbasaur, brasas );
    }

    @Test(expected= AtaqueAgotadoException.class)
    public void test48CharmanderAtacaConBrasasAChanseyHastaQuedarseSinMasAtaques() {
        AlgoMon charmander  = new Charmander();
        AlgoMon chansey   	= new Chansey();

        Ataque brasas		= new Brasas();

        Boolean NoAttack = brasas.poseeAtaques();
        
        while (! NoAttack ) {
        	charmander.attack( chansey , brasas);
        	NoAttack = brasas.poseeAtaques();
        }
        
        assertEquals( 0, chansey.getLife());
        
        charmander.attack( chansey, brasas );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test49CharmanderAtacaConBrasasAJigglypuffHastaQuedarseSinMasAtaques() {
        AlgoMon charmander  	= new Charmander();
        AlgoMon jigglypuff   	= new Jigglypuff();

        Ataque brasas			= new Brasas();

        Boolean NoAttack = brasas.poseeAtaques();
        
        while (! NoAttack ) {
        	charmander.attack( jigglypuff, brasas );
        	NoAttack = brasas.poseeAtaques();
        }
        
        assertEquals( 0, jigglypuff.getLife());
        
        charmander.attack( jigglypuff, brasas );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test50CharmanderAtacaConBrasasARattataHastaQuedarseSinMasAtaques() {
        AlgoMon charmander  	= new Charmander();
        AlgoMon rattata   		= new Rattata();

        Ataque brasas     		= new Brasas();

        Boolean NoAttack = brasas.poseeAtaques();
        
        while (! NoAttack ) {
        	charmander.attack( rattata ,brasas);
        	NoAttack = brasas.poseeAtaques();
        }
        
        assertEquals( 10, rattata.getLife());
        
        charmander.attack( rattata, brasas );
    }

    @Test(expected= AtaqueAgotadoException.class)
    public void test51CharmanderAtacaConBrasasASquirtleHastaQuedarseSinMasAtaques() {
        AlgoMon charmander  = new Charmander();
        AlgoMon squirtle   	= new Squirtle();

        Ataque brasas		= new Brasas();

        Boolean NoAttack = brasas.poseeAtaques();
        
        while (! NoAttack ) {
        	charmander.attack( squirtle , brasas);
        	NoAttack = brasas.poseeAtaques();
        }
        
        assertEquals( 70, squirtle.getLife());
        
        charmander.attack( squirtle, brasas );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test52CharmanderAtacaConBrasasACharmanderHastaQuedarseSinMasAtaques() {
        AlgoMon charmander  	= new Charmander();
        AlgoMon otroCharmander	= new Charmander();

        Ataque brasas			= new Brasas();

        Boolean NoAttack = brasas.poseeAtaques();
        
        while (! NoAttack ) {
        	charmander.attack( otroCharmander, brasas );
        	NoAttack = brasas.poseeAtaques();
        }
        
        assertEquals( 90, otroCharmander.getLife());
        
        charmander.attack( otroCharmander, brasas );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test53SquirtleAtacaConBurbujaABulbasuarHastaQuedarseSinMasAtaques() {
        AlgoMon squirtle  		= new Squirtle();
        AlgoMon bulbasaur		= new Bulbasaur();

        Ataque burbuja     = new Burbuja();

        Boolean NoAttack = burbuja.poseeAtaques();
        
        while (! NoAttack ) {
        	squirtle.attack( bulbasaur, burbuja );
        	NoAttack = burbuja.poseeAtaques();
        }
        
        assertEquals( 65, bulbasaur.getLife());
        
        squirtle.attack( bulbasaur, burbuja );
    }

    @Test(expected= AtaqueAgotadoException.class)
    public void test54SquirtleAtacaConBurbujaAChanseyHastaQuedarseSinMasAtaques() {
        AlgoMon squirtle	= new Squirtle();
        AlgoMon chansey   	= new Chansey();

        Ataque burbuja		= new Burbuja();

        Boolean NoAttack = burbuja.poseeAtaques();
        
        while (! NoAttack ) {
        	squirtle.attack( chansey , burbuja);
        	NoAttack = burbuja.poseeAtaques();
        }
        
        assertEquals( 0, chansey.getLife());
        
        squirtle.attack( chansey, burbuja );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test55SquirtleAtacaConBurbujaACharmanderHastaQuedarseSinMasAtaques() {
        AlgoMon squirtle		= new Squirtle();
        AlgoMon charmander		= new Charmander();

        Ataque burbuja			= new Burbuja();

        Boolean NoAttack = burbuja.poseeAtaques();
        
        while (! NoAttack ) {
        	squirtle.attack( charmander, burbuja );
        	NoAttack = burbuja.poseeAtaques();
        }
        
        assertEquals( 0, charmander.getLife());
        
        squirtle.attack( charmander, burbuja );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test56SquirtleAtacaConBurbujaAJigglypuffHastaQuedarseSinMasAtaques() {
        AlgoMon squirtle		= new Squirtle();
        AlgoMon jigglypuff   	= new Jigglypuff();

        Ataque burbuja			= new Burbuja();

        Boolean NoAttack = burbuja.poseeAtaques();
        
        while (! NoAttack ) {
        	squirtle.attack( jigglypuff , burbuja);
        	NoAttack = burbuja.poseeAtaques();
        }
        
        assertEquals( 0, jigglypuff.getLife());
        
        squirtle.attack( jigglypuff , burbuja);
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test56SquirtleAtacaConBurbujaARattataHastaQuedarseSinMasAtaques() {
        AlgoMon squirtle  		= new Squirtle();
        AlgoMon rattata   		= new Rattata();

        Ataque burbuja     		= new Burbuja();

        Boolean NoAttack = burbuja.poseeAtaques();
        
        while (! NoAttack ) {
        	squirtle.attack( rattata , burbuja);
        	NoAttack = burbuja.poseeAtaques();
        }
        
        assertEquals( 20, rattata.getLife());
        
        squirtle.attack( rattata, burbuja );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test57SquirtleAtacaConBurbujaASquirtleHastaQuedarseSinMasAtaques() {
        AlgoMon squirtle  		= new Squirtle();
        AlgoMon otrosquirtle   	= new Squirtle();

        Ataque burbuja			= new Burbuja();

        Boolean NoAttack = burbuja.poseeAtaques();
        
        while (! NoAttack ) {
        	squirtle.attack( otrosquirtle, burbuja );
        	NoAttack = burbuja.poseeAtaques();
        }
        
        assertEquals( 75, otrosquirtle.getLife());
        
        squirtle.attack( otrosquirtle, burbuja );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test58SquirtleAtacaConCanionDeAguaABulbasuarHastaQuedarseSinMasAtaques() {
        AlgoMon squirtle  		= new Squirtle();
        AlgoMon bulbasaur		= new Bulbasaur();

        Ataque canionDeAgua     = new CanionDeAgua();

        Boolean NoAttack = canionDeAgua.poseeAtaques();
        
        while (! NoAttack ) {
        	squirtle.attack( bulbasaur , canionDeAgua);
        	NoAttack = canionDeAgua.poseeAtaques();
        }
        
        assertEquals( 60, bulbasaur.getLife());
        
        squirtle.attack( bulbasaur, canionDeAgua );
    }

    @Test(expected= AtaqueAgotadoException.class)
    public void test59SquirtleAtacaConCanionDeAguaAChanseyHastaQuedarseSinMasAtaques() {
        AlgoMon squirtle	= new Squirtle();
        AlgoMon chansey   	= new Chansey();

        Ataque canionDeAgua	= new CanionDeAgua();

        Boolean NoAttack = canionDeAgua.poseeAtaques();
        
        while (! NoAttack ) {
        	squirtle.attack( chansey , canionDeAgua);
        	NoAttack = canionDeAgua.poseeAtaques();
        }
        
        assertEquals( 0, chansey.getLife());
        
        squirtle.attack( chansey, canionDeAgua );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test60SquirtleAtacaConCanionDeAguaACharmanderHastaQuedarseSinMasAtaques() {
        AlgoMon squirtle		= new Squirtle();
        AlgoMon charmander		= new Charmander();

        Ataque canionDeAgua		= new CanionDeAgua();

        Boolean NoAttack = canionDeAgua.poseeAtaques();
        
        while (! NoAttack ) {
        	squirtle.attack( charmander , canionDeAgua);
        	NoAttack = canionDeAgua.poseeAtaques();
        }
        
        assertEquals( 0, charmander.getLife());
        
        squirtle.attack( charmander, canionDeAgua );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test61SquirtleAtacaConCanionDeAguaAJigglypuffHastaQuedarseSinMasAtaques() {
        AlgoMon squirtle		= new Squirtle();
        AlgoMon jigglypuff   	= new Jigglypuff();

        Ataque canionDeAgua		= new CanionDeAgua();

        Boolean NoAttack = canionDeAgua.poseeAtaques();
        
        while (! NoAttack ) {
        	squirtle.attack( jigglypuff, canionDeAgua );
        	NoAttack = canionDeAgua.poseeAtaques();
        }
        
        assertEquals( 0, jigglypuff.getLife());
        
        squirtle.attack( jigglypuff, canionDeAgua );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test62SquirtleAtacaConCanionDeAguaARattataHastaQuedarseSinMasAtaques() {
        AlgoMon squirtle  		= new Squirtle();
        AlgoMon rattata   		= new Rattata();

        Ataque canionDeAgua		= new CanionDeAgua();

        Boolean NoAttack = canionDeAgua.poseeAtaques();
        
        while (! NoAttack ) {
        	squirtle.attack( rattata , canionDeAgua);
        	NoAttack = canionDeAgua.poseeAtaques();
        }
        
        assertEquals( 10, rattata.getLife());
        
        squirtle.attack( rattata, canionDeAgua );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test63SquirtleAtacaConCanionDeAguaASquirtleHastaQuedarseSinMasAtaques() {
        AlgoMon squirtle  		= new Squirtle();
        AlgoMon otroSquirtle   	= new Squirtle();

        Ataque canionDeAgua		= new CanionDeAgua();

        Boolean NoAttack = canionDeAgua.poseeAtaques();
        
        while (! NoAttack ) {
        	squirtle.attack( otroSquirtle ,canionDeAgua);
        	NoAttack = canionDeAgua.poseeAtaques();
        }
        
        assertEquals( 70, otroSquirtle.getLife());
        
        squirtle.attack( otroSquirtle, canionDeAgua );
    }

    
    @Test(expected= AtaqueAgotadoException.class)
    public void test64ChanseyAtacaConCantoABulbasuarHastaQuedarseSinMasAtaques() {
        AlgoMon chansey			= new Chansey();
        AlgoMon bulbasaur		= new Bulbasaur();

        Ataque canto 			= new Canto();

        Boolean NoAttack = canto.poseeAtaques();
        
        while (! NoAttack ) {
        	chansey.attack( bulbasaur, canto );
        	NoAttack = canto.poseeAtaques();
        }
        
        assertEquals( 140, bulbasaur.getLife());
        
        chansey.attack( bulbasaur, canto );
    }

    @Test(expected= AtaqueAgotadoException.class)
    public void test65ChanseyAtacaConCantoAChanseyHastaQuedarseSinMasAtaques() {
        AlgoMon chansey			= new Chansey();
        AlgoMon otroChansey   	= new Chansey();

        Ataque canto			= new Canto();

        Boolean NoAttack = canto.poseeAtaques();
        
        while (! NoAttack ) {
        	chansey.attack( otroChansey, canto );
        	NoAttack = canto.poseeAtaques();
        }
        
        assertEquals( 130, otroChansey.getLife());
        
        chansey.attack( otroChansey, canto );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test66ChanseyAtacaConCantoACharmanderHastaQuedarseSinMasAtaques() {
        AlgoMon chansey			= new Chansey();
        AlgoMon charmander		= new Charmander();

        Ataque canto			= new Canto();

        Boolean NoAttack = canto.poseeAtaques();
        
        while (! NoAttack ) {
        	chansey.attack( charmander, canto );
        	NoAttack = canto.poseeAtaques();
        }
        
        assertEquals( 170, charmander.getLife());
        
        chansey.attack( charmander, canto );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test67ChanseyAtacaConCantoAJigglypuffHastaQuedarseSinMasAtaques() {
        AlgoMon chansey			= new Chansey();
        AlgoMon jigglypuff   	= new Jigglypuff();

        Ataque canto		= new Canto();

        Boolean NoAttack = canto.poseeAtaques();
        
        while (! NoAttack ) {
        	chansey.attack( jigglypuff, canto );
        	NoAttack = canto.poseeAtaques();
        }
        
        assertEquals( 130, jigglypuff.getLife());
        
        chansey.attack( jigglypuff, canto );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test68ChanseyAtacaConCantoARattataHastaQuedarseSinMasAtaques() {
        AlgoMon chansey			= new Chansey();
        AlgoMon rattata   		= new Rattata();

        Ataque canto		= new Canto();

        Boolean NoAttack = canto.poseeAtaques();
        
        while (! NoAttack ) {
        	chansey.attack( rattata , canto);
        	NoAttack = canto.poseeAtaques();
        }
        
        assertEquals( 170, rattata.getLife());
        
        chansey.attack( rattata, canto );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test69ChanseyAtacaConCantoASquirtleHastaQuedarseSinMasAtaques() {
        AlgoMon chansey  	= new Chansey();
        AlgoMon squirtle   	= new Squirtle();

        Ataque canto		= new Canto();

        Boolean NoAttack = canto.poseeAtaques();
        
        while (! NoAttack ) {
        	chansey.attack( squirtle, canto );
        	NoAttack = canto.poseeAtaques();
        }
        
        assertEquals( 150, squirtle.getLife());
        
        chansey.attack( squirtle, canto );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test70BulbasaurAtacaConChupavidasABulbasuarHastaQuedarseSinMasAtaques() {
        AlgoMon bulbasaur			= new Bulbasaur();
        AlgoMon otroBulbasaur		= new Bulbasaur();

        Ataque chupavidas 			= new Chupavidas(bulbasaur);

        Boolean NoAttack = chupavidas.poseeAtaques();
        
        while (! NoAttack ) {
            bulbasaur.attack( otroBulbasaur, chupavidas);
        	NoAttack = chupavidas.poseeAtaques();
        }
        
        assertEquals( 84, otroBulbasaur.getLife());
        
        bulbasaur.attack( otroBulbasaur, chupavidas );
    }

    @Test(expected= AtaqueAgotadoException.class)
    public void test71BulbasaurAtacaConChupavidasAChanseyHastaQuedarseSinMasAtaques() {
        AlgoMon bulbasaur		= new Bulbasaur();
        AlgoMon chansey   		= new Chansey();

        Ataque chupavidas		= new Chupavidas(bulbasaur);

        Boolean NoAttack = chupavidas.poseeAtaques();
        
        while (! NoAttack ) {
        	bulbasaur.attack( chansey , chupavidas);
        	NoAttack = chupavidas.poseeAtaques();
        }
        
        assertEquals( 10, chansey.getLife());
        
        bulbasaur.attack( chansey, chupavidas );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test72BulbasaurAtacaConChupavidasACharmanderHastaQuedarseSinMasAtaques() {
        AlgoMon bulbasaur		= new Bulbasaur();
        AlgoMon charmander		= new Charmander();

        Ataque chupavidas		= new Chupavidas(bulbasaur);

        Boolean NoAttack = chupavidas.poseeAtaques();
        
        while (! NoAttack ) {
        	bulbasaur.attack( charmander , chupavidas);
        	NoAttack = chupavidas.poseeAtaques();
        }
        
        assertEquals( 114, charmander.getLife());
        
        bulbasaur.attack( charmander, chupavidas );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test73BulbasaurAtacaConChupavidasAJigglypuffHastaQuedarseSinMasAtaques() {
        AlgoMon bulbasaur		= new Bulbasaur();
        AlgoMon jigglypuff   	= new Jigglypuff();

        Ataque chupavidas		= new Chupavidas(bulbasaur);

        Boolean NoAttack = chupavidas.poseeAtaques();
        
        while (! NoAttack ) {
        	bulbasaur.attack( jigglypuff, chupavidas );
        	NoAttack = chupavidas.poseeAtaques();
        }
        
        assertEquals( 10, jigglypuff.getLife());
        
        bulbasaur.attack( jigglypuff, chupavidas );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test74BulbasaurAtacaConChupavidasARattataHastaQuedarseSinMasAtaques() {
        AlgoMon bulbasaur		= new Bulbasaur();
        AlgoMon rattata   		= new Rattata();

        Ataque chupavidas 		= new Chupavidas(bulbasaur);

        Boolean NoAttack = chupavidas.poseeAtaques();
        
        while (! NoAttack ) {
        	bulbasaur.attack( rattata , chupavidas);
        	NoAttack = chupavidas.poseeAtaques();
        }
        
        assertEquals( 50, rattata.getLife());
        
        bulbasaur.attack( rattata, chupavidas );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test75BulbasaurAtacaConChupavidasASquirtleHastaQuedarseSinMasAtaques() {
        AlgoMon bulbasaur	= new Bulbasaur();
        AlgoMon squirtle   	= new Squirtle();

        Ataque chupavidas	= new Chupavidas(bulbasaur);

        Boolean NoAttack = chupavidas.poseeAtaques();
        
        while (! NoAttack ) {
        	bulbasaur.attack( squirtle , chupavidas);
        	NoAttack = chupavidas.poseeAtaques();
        }
        
        assertEquals( 0, squirtle.getLife());
        
        bulbasaur.attack( squirtle , chupavidas);
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test76BulbasaurAtacaConLatigoCepaABulbasuarHastaQuedarseSinMasAtaques() {
        AlgoMon bulbasaur			= new Bulbasaur();
        AlgoMon otroBulbasaur		= new Bulbasaur();

        Ataque latigoCepa 			= new LatigoCepa();

        Boolean NoAttack = latigoCepa.poseeAtaques();

        while (! NoAttack ) {
            bulbasaur.attack( otroBulbasaur, latigoCepa);
        	NoAttack = latigoCepa.poseeAtaques();
        }

        assertEquals( 70, otroBulbasaur.getLife());
        
        bulbasaur.attack( otroBulbasaur, latigoCepa );
    }

    @Test(expected= AtaqueAgotadoException.class)
    public void test77BulbasaurAtacaConLatigoCepaAChanseyHastaQuedarseSinMasAtaques() {
        AlgoMon bulbasaur		= new Bulbasaur();
        AlgoMon chansey   		= new Chansey();

        Ataque latigoCepa		= new LatigoCepa();

        Boolean NoAttack = latigoCepa.poseeAtaques();
        
        while (! NoAttack ) {
        	bulbasaur.attack( chansey , latigoCepa);
        	NoAttack = latigoCepa.poseeAtaques();
        }
        
        assertEquals( 0, chansey.getLife());
        
        bulbasaur.attack( chansey , latigoCepa);
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test78BulbasaurAtacaConLatigoCepaACharmanderHastaQuedarseSinMasAtaques() {
        AlgoMon bulbasaur		= new Bulbasaur();
        AlgoMon charmander		= new Charmander();

        Ataque latigoCepa		= new LatigoCepa();

        Boolean NoAttack = latigoCepa.poseeAtaques();
        
        while (! NoAttack ) {
        	bulbasaur.attack( charmander , latigoCepa);
        	NoAttack = latigoCepa.poseeAtaques();
        }
        
        assertEquals( 100, charmander.getLife());
        
        bulbasaur.attack( charmander, latigoCepa );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test79BulbasaurAtacaConLatigoCepaAJigglypuffHastaQuedarseSinMasAtaques() {
        AlgoMon bulbasaur		= new Bulbasaur();
        AlgoMon jigglypuff   	= new Jigglypuff();

        Ataque latigoCepa		= new LatigoCepa();

        Boolean NoAttack = latigoCepa.poseeAtaques();
        
        while (! NoAttack ) {
        	bulbasaur.attack( jigglypuff , latigoCepa);
        	NoAttack = latigoCepa.poseeAtaques();
        }
        
        assertEquals( 0, jigglypuff.getLife());
        
        bulbasaur.attack( jigglypuff, latigoCepa );
    }
    
    @Test(expected= AtaqueAgotadoException.class)
    public void test80BulbasaurAtacaConLatigoCepaARattataHastaQuedarseSinMasAtaques() {
        AlgoMon bulbasaur		= new Bulbasaur();
        AlgoMon rattata   		= new Rattata();

        Ataque latigoCepa 		= new LatigoCepa();

        Boolean NoAttack = latigoCepa.poseeAtaques();
        
        while (! NoAttack ) {
        	bulbasaur.attack( rattata , latigoCepa);
        	NoAttack = latigoCepa.poseeAtaques();
        }
        
        assertEquals( 20, rattata.getLife());
        
        bulbasaur.attack( rattata, latigoCepa );
    }
    
    @Test(expected = AtaqueAgotadoException.class)
    public void test81BulbasaurAtacaConLatigoCepaASquirtleHastaQuedarseSinMasAtaques() {
        AlgoMon bulbasaur	= new Bulbasaur();
        AlgoMon squirtle   	= new Squirtle();

        Ataque latigoCepa	= new LatigoCepa();

        Boolean NoAttack = latigoCepa.poseeAtaques();
        
        while (! NoAttack ) {
        	bulbasaur.attack( squirtle , latigoCepa);
        	NoAttack = latigoCepa.poseeAtaques();
        }
        
        assertEquals( 0, squirtle.getLife());
        
        bulbasaur.attack( squirtle, latigoCepa );
    }
    // 2da Entrega
    // PUNTO 2.1
    @Test
    public void test82JigglypuffAtacaConCantoYElAlgomonAtacadoNoPuedeAtacarPorTresTurnos(){
        AlgoMon jigglypuff = new Jigglypuff();
        AlgoMon charmander = new Charmander();

        Ataque canto = new Canto();
        Ataque brasas = new Brasas();

        jigglypuff.attack(charmander, canto);

        int vidaJigglypuff = jigglypuff.getLife();
        for (int i = 0 ; i < 3 ; i++ ) {
            try {
                charmander.attack(jigglypuff, brasas);
            }catch (AlgomonDormidoException e) {}
            assertEquals(vidaJigglypuff, jigglypuff.getLife());
        }

        charmander.attack(jigglypuff, brasas);
        assertEquals(114, jigglypuff.getLife());
    }

    @Test
    public void test83ChanseyAtacaConCantoYElAlgomonAtacadoNoPuedeAtacarPorTresTurnos(){
        AlgoMon Chansey = new Chansey();
        AlgoMon charmander = new Charmander();

        Ataque canto = new Canto();
        Ataque brasas = new Brasas();

        Chansey.attack(charmander, canto);
        int vidaChansey = Chansey.getLife();
        for (int i = 0 ; i < 3; i++) {
            try {
                charmander.attack(Chansey, brasas);
            }catch (AlgomonDormidoException e){}
            assertEquals( vidaChansey, Chansey.getLife());
        }

        charmander.attack(Chansey, brasas);
        assertEquals(114, Chansey.getLife());
    }

    //PUNTO 2.2
    @Test
    public void test84BulbasaurAtacaConChupavidasACharmanderYLeQuita7PuntosYGana2PuntosDeVida(){
        AlgoMon bulbasaur = new Bulbasaur();
        AlgoMon charmander = new Charmander();

        Ataque chupavidas = new Chupavidas(bulbasaur);

        bulbasaur.attack(charmander, chupavidas);

        assertEquals(163, charmander.getLife());
        assertEquals(142,bulbasaur.getLife());
    }
    //PUNTO 2.3
    @Test
    public void test85BulbasaurAtacaConChupavidasASquirtleYLeQuita30PuntosYGana9PuntosDeVida(){
        AlgoMon bulbasaur = new Bulbasaur();
        AlgoMon squirtle = new Squirtle();

        Ataque chupavidas = new Chupavidas(bulbasaur);

        bulbasaur.attack(squirtle, chupavidas);

        assertEquals(120, squirtle.getLife());
        assertEquals(149,bulbasaur.getLife());
    }

    //PUNTO 2.3 BIS
    @Test
    public void test86BulbasaurAtacaConChupavidasABulbasaurYLeQuita7PuntosYGana2PuntosDeVida(){
        AlgoMon bulbasaur = new Bulbasaur();
        AlgoMon otroBulba = new Bulbasaur();

        Ataque chupavidas = new Chupavidas(bulbasaur);

        bulbasaur.attack(otroBulba, chupavidas);

        assertEquals(133, otroBulba.getLife());
        assertEquals(142,bulbasaur.getLife());
    }

    //PUNTO 2.4
    @Test
    public void test87BulbasaurAtacaConChupavidasARattataYLeQuita15PuntosYGana4PuntosDeVida(){
        AlgoMon bulbasaur = new Bulbasaur();
        AlgoMon rattata = new Rattata();

        Ataque chupavidas = new Chupavidas(bulbasaur);

        bulbasaur.attack(rattata, chupavidas);

        assertEquals(155, rattata.getLife());
        assertEquals(144,bulbasaur.getLife());
    }

    @Test
    public void test88BulbasaurAtacaConChupavidasAChanseyYLeQuita15PuntosYGana4PuntosDeVida(){
        AlgoMon bulbasaur = new Bulbasaur();
        AlgoMon chansey = new Chansey();

        Ataque chupavidas = new Chupavidas(bulbasaur);

        bulbasaur.attack(chansey, chupavidas);

        assertEquals(115, chansey.getLife());
        assertEquals(144,bulbasaur.getLife());
    }

    @Test
    public void test89BulbasaurAtacaConChupavidasAJigglypuffYLeQuita15PuntosYGana4PuntosDeVida(){
        AlgoMon bulbasaur = new Bulbasaur();
        AlgoMon jigglypuff = new Jigglypuff();

        Ataque chupavidas = new Chupavidas(bulbasaur);

        bulbasaur.attack(jigglypuff, chupavidas);

        assertEquals(115, jigglypuff.getLife());
        assertEquals(144,bulbasaur.getLife());
    }

    //PUNTO 2.5
    @Test
    public void test90CharmanderAtacaConFogonazoASquirtleYSeQuema(){
        AlgoMon charmander = new Charmander();
        AlgoMon squirtle = new Squirtle();

        Ataque fogonazo = new Fogonazo();
        Ataque burbuja = new Burbuja();

        charmander.attack(squirtle, fogonazo);

        squirtle.attack(charmander, burbuja);
        squirtle.attack(charmander, burbuja);
        squirtle.attack(charmander, burbuja);

        assertEquals(104,squirtle.getLife());
    }

    @Test
    public void test91CharmanderAtacaConFogonazoACharmanderYSeQuema(){
        AlgoMon charmander = new Charmander();
        AlgoMon otroCharmander = new Charmander();

        Ataque fogonazo = new Fogonazo();

        charmander.attack(otroCharmander, fogonazo);

        otroCharmander.attack(charmander, fogonazo);
        otroCharmander.attack(charmander, fogonazo);
        otroCharmander.attack(charmander, fogonazo);

        assertEquals(118,otroCharmander.getLife());
    }

    @Test
    public void test92CharmanderAtacaConFogonazoABulbasaurYSeQuema(){
        AlgoMon charmander = new Charmander();
        AlgoMon bulbasaur = new Bulbasaur();

        Ataque fogonazo = new Fogonazo();
        Ataque latigoCepa = new LatigoCepa();

        charmander.attack(bulbasaur, fogonazo);

        bulbasaur.attack(charmander, latigoCepa);
        bulbasaur.attack(charmander, latigoCepa);
        bulbasaur.attack(charmander, latigoCepa);

        assertEquals(94,bulbasaur.getLife());
    }

    @Test
    public void test93CharmanderAtacaConFogonazoAChanseyYSeQuema(){
        AlgoMon charmander = new Charmander();
        AlgoMon chansey = new Chansey();

        Ataque fogonazo = new Fogonazo();
        Ataque ataqueRapido = new AtaqueRapido();

        charmander.attack(chansey, fogonazo);

        chansey.attack(charmander, ataqueRapido);
        chansey.attack(charmander, ataqueRapido);
        chansey.attack(charmander, ataqueRapido);

        assertEquals(89,chansey.getLife());
    }

    @Test
    public void test94CharmanderAtacaConFogonazoARattataYSeQuema(){
        AlgoMon charmander = new Charmander();
        AlgoMon rattata = new Rattata();

        Ataque fogonazo = new Fogonazo();
        Ataque ataqueRapido = new AtaqueRapido();

        charmander.attack(rattata, fogonazo);

        rattata.attack(charmander, ataqueRapido);
        rattata.attack(charmander, ataqueRapido);
        rattata.attack(charmander, ataqueRapido);

        assertEquals(117,rattata.getLife());
    }

    @Test
    public void test94CharmanderAtacaConFogonazoAJigglypuffYSeQuema(){
        AlgoMon charmander = new Charmander();
        AlgoMon jigglypuff = new Jigglypuff();

        Ataque fogonazo = new Fogonazo();
        Ataque ataqueRapido = new AtaqueRapido();

        charmander.attack(jigglypuff, fogonazo);

        jigglypuff.attack(charmander, ataqueRapido);
        jigglypuff.attack(charmander, ataqueRapido);
        jigglypuff.attack(charmander, ataqueRapido);

        assertEquals(89,jigglypuff.getLife());
    }

    @Test
    public void test95RattataAtacaConFogonazoASquirtleYSeQuema(){
        AlgoMon rattata = new Rattata();
        AlgoMon squirtle = new Squirtle();

        Ataque fogonazo = new Fogonazo();
        Ataque burbuja = new Burbuja();

        rattata.attack(squirtle, fogonazo);

        squirtle.attack(rattata, burbuja);
        squirtle.attack(rattata, burbuja);
        squirtle.attack(rattata, burbuja);

        assertEquals(104,squirtle.getLife());
    }

    @Test
    public void test96RattataAtacaConFogonazoACharmanderYSeQuema(){
        AlgoMon rattata = new Rattata();
        AlgoMon charmander = new Charmander();

        Ataque fogonazo = new Fogonazo();

        rattata.attack(charmander, fogonazo);

        charmander.attack(rattata, fogonazo);
        charmander.attack(rattata, fogonazo);
        charmander.attack(rattata, fogonazo);

        assertEquals(118,charmander.getLife());
    }

    @Test
    public void test97RattataAtacaConFogonazoABulbasaurYSeQuema(){
        AlgoMon rattata = new Rattata();
        AlgoMon bulbasaur = new Bulbasaur();

        Ataque fogonazo = new Fogonazo();
        Ataque latigoCepa = new LatigoCepa();

        rattata.attack(bulbasaur, fogonazo);

        bulbasaur.attack(rattata, latigoCepa);
        bulbasaur.attack(rattata, latigoCepa);
        bulbasaur.attack(rattata, latigoCepa);

        assertEquals(94,bulbasaur.getLife());
    }

    @Test
    public void test98RattataAtacaConFogonazoAChanseyYSeQuema(){
        AlgoMon rattata = new Rattata();
        AlgoMon chansey = new Chansey();

        Ataque fogonazo = new Fogonazo();
        Ataque ataqueRapido = new AtaqueRapido();

        rattata.attack(chansey, fogonazo);

        chansey.attack(rattata, ataqueRapido);
        chansey.attack(rattata, ataqueRapido);
        chansey.attack(rattata, ataqueRapido);

        assertEquals(89,chansey.getLife());
    }

    @Test
    public void test99RattataAtacaConFogonazoAJigglypuffYSeQuema(){
        AlgoMon rattata = new Rattata();
        AlgoMon jigglypuff = new Jigglypuff();

        Ataque fogonazo = new Fogonazo();
        Ataque ataqueRapido = new AtaqueRapido();

        rattata.attack(jigglypuff, fogonazo);

        jigglypuff.attack(rattata, ataqueRapido);
        jigglypuff.attack(rattata, ataqueRapido);
        jigglypuff.attack(rattata, ataqueRapido);

        assertEquals(89,jigglypuff.getLife());
    }

    @Test
    public void test100RattataAtacaConFogonazoARattataYSeQuema(){
        AlgoMon rattata = new Rattata();
        AlgoMon otroRattata = new Rattata();

        Ataque fogonazo = new Fogonazo();
        Ataque ataqueRapido = new AtaqueRapido();

        rattata.attack(otroRattata, fogonazo);

        otroRattata.attack(rattata, ataqueRapido);
        otroRattata.attack(rattata, ataqueRapido);
        otroRattata.attack(rattata, ataqueRapido);

        assertEquals(117,otroRattata.getLife());
    }

    // 3er Entrega

    @Test(expected = AlgomonMuertoException.class)
    public void test101CharmanderAtacaConFogonazoARattataYLuegoChanseyAtacaARattataConCantoRattataNoPuedeAtacarPorTresTurnosYMuereLuegoDeHacerVariosAtaques() {
        AlgoMon charmander = new Charmander();
        AlgoMon rattata = new Rattata();
        AlgoMon chansey = new Chansey();

        charmander.attack(rattata, new Fogonazo());
        chansey.attack( rattata, new Canto());

        Ataque ataque = new AtaqueRapido();

        for (int i= 0; i< 3; i++) {
            try {
                rattata.attack(charmander , ataque);
            }catch (AlgoMonNoPuedeAtacarException e){}
        }
        while (true) {
            rattata.attack(charmander, ataque);
        }

    }

    @Test(expected = AlgomonMuertoException.class)
    public void test102ChanseyAtacaARattataConCantoYLuegoCharmanderAtacaConFogonazoARattataYRatataNoPuedeAtacarPorTresTurnosYMuereLuegoDeHacerVariosAtaques() {
        AlgoMon chansey     = new Chansey();
        AlgoMon charmander  = new Charmander();
        AlgoMon rattata     = new Rattata();

        chansey.attack( rattata, new Canto());
        charmander.attack(rattata, new Fogonazo());

        Ataque ataque = new AtaqueRapido();

        for (int i = 0; i < 3 ; i++) {
            try {
                rattata.attack(charmander, ataque);
            }catch (AlgoMonNoPuedeAtacarException e) {}
        }
        while (true) {
            rattata.attack( charmander, ataque );
        }
    }

    @Test
    public void test103RattataEsAtacadoPorFogonazoYPorCantoLuegoAtaca4VecesACharmander() {
        AlgoMon charmander  = new Charmander();
        AlgoMon rattata     = new Rattata();

        rattata.attackedBy( new Fogonazo() );
        rattata.attackedBy( new Canto() );

        int vida = rattata.getLife();
        Ataque ataque = new AtaqueRapido();

        for (int i = 0; i < 3; i++) {
            try {
                rattata.attack( charmander, ataque);
            }catch ( AlgoMonNoPuedeAtacarException e ){}
            assertEquals(vida -= 17 , rattata.getLife());
            assertEquals(170, charmander.getLife());
        }

        rattata.attack(charmander, ataque);
        assertEquals(160, charmander.getLife());
        assertEquals(vida -= 17, rattata.getLife());
    }

    @Test
    public void test104RattataEsAtacadoPorCantoYPorFogonazoYLuegoAtaca4VecesACharmander() {
        AlgoMon charmander  = new Charmander();
        AlgoMon rattata     = new Rattata();

        rattata.attackedBy( new Canto() );
        rattata.attackedBy( new Fogonazo() );

        int vida = rattata.getLife();
        Ataque ataque = new AtaqueRapido();

        for (int i = 0; i < 3; i++) {
            try {
                rattata.attack( charmander, ataque);
            }catch ( AlgoMonNoPuedeAtacarException e ){}
            assertEquals(vida -= 17 , rattata.getLife());
        }

        rattata.attack(charmander, ataque);
        assertEquals(160, charmander.getLife());
        assertEquals(vida -= 17, rattata.getLife());
    }
}