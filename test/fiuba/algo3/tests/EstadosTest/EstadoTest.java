package fiuba.algo3.tests.EstadosTest;

import fiuba.algo3.modelo.algomon.*;
import fiuba.algo3.modelo.ataque.*;
import fiuba.algo3.modelo.estados.*;
import fiuba.algo3.modelo.excepciones.*;
import org.junit.Test;


public class EstadoTest {

    @Test(expected = AlgomonDormidoException.class)
    public void test01EstadoDormidoLanzaExcepcionAlIntentarAtacar(){
        Estado estadoDormido = new EstadoDormido();

        estadoDormido.attack(new Chansey(),new AtaqueRapido(), new Bulbasaur());
    }

    @Test(expected = EstadoNoDormido.class)
    public void test02EstadoDormidoPuedeAtacarDespuesDe3Turnos(){
        Estado estadoDormido = new EstadoDormido();

        try{
            estadoDormido.attack(new Chansey(),new AtaqueRapido(), new Bulbasaur());
        }catch(AlgomonDormidoException e){}
        try{
            estadoDormido.attack(new Chansey(),new AtaqueRapido(), new Bulbasaur());
        }catch(AlgomonDormidoException e){}
        try{
            estadoDormido.attack(new Chansey(),new AtaqueRapido(), new Bulbasaur());
        }catch(AlgomonDormidoException e){}
        estadoDormido.attack(new Chansey(),new AtaqueRapido(), new Bulbasaur());
    }

    @Test(expected = AlgomonMuertoException.class)
    public void test03EstadoMuertoLanzaExcepcionAlIntentarAtacar(){
        Estado estadoMuerto = new EstadoMuerto();

        estadoMuerto.attack(new Chansey(),new AtaqueRapido(), new Bulbasaur());
    }

}
