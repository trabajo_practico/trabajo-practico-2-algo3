package fiuba.algo3.tests.EstadosTest;

import fiuba.algo3.modelo.algomon.*;
import fiuba.algo3.modelo.estados.*;
import fiuba.algo3.modelo.excepciones.ErrorCreandoEstadoCompuestoException;
import org.junit.Test;

import static org.junit.Assert.*;


public class EstadoCompuestoTest {

    @Test(expected = ErrorCreandoEstadoCompuestoException.class)
    public void test01CreoUnEstadoCompuestoNoPermitido() {
        Estado estado = new EstadoCompuesto( new EstadoQuemado(), new EstadoQuemado() );
    }

    @Test
    public void test02CreoUnEstadoCompuestoYEliminoUnoDeLosEstados() {
        EstadoCompuesto estado = new EstadoCompuesto( new EstadoQuemado(), new EstadoDormido());

        estado.eliminar(new EstadoDormido());
    }

    @Test
    public void test03CreoUnEstadoCompuestoYSeteoOtroEstadoAUnAlgomon() {
        AlgoMon algoMon = new Charmander();

        Estado estado = new EstadoCompuesto( new EstadoDormido(), new EstadoQuemado());
        estado.setEstado( algoMon, new EstadoMuerto() );
    }
}