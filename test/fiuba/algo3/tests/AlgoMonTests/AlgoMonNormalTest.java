package fiuba.algo3.tests.AlgoMonTests;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.modelo.algomon.*;
import fiuba.algo3.modelo.ataque.*;

public class AlgoMonNormalTest {

	@Test
	public void test01AtaqueDeFuegoSacaPuntosDeVidaSinMultiplicador(){
		AlgoMon charmander 		= new Charmander();
		AlgoMon chansey			= new Chansey();
		
		Ataque brasas			= new Brasas();
		
		charmander.attack(chansey, brasas);
		
		//debe sacar los 16 puntos de vida usual por el ataque
		assertEquals(114, chansey.getLife());
		
	}
	
	@Test
	public void test02AtaqueDeAguaSacaPuntosDeVidaSinMultiplicador(){
		AlgoMon squirtle 		= new Squirtle();
		AlgoMon chansey			= new Chansey();
		
		Ataque burbuja 			= new Burbuja();
		
		squirtle.attack(chansey, burbuja);
		
		//debe sacar los 10 puntos de vida usual por el ataque
		assertEquals(120, chansey.getLife());
		
	}
	
	@Test
	public void test03AtaqueDePlantaSacaPuntosDeVidaSinMultiplicador(){
		AlgoMon bulbasaur		= new Bulbasaur();
		AlgoMon chansey			= new Chansey();
		
		Ataque latigoCepa		= new LatigoCepa();
		
		bulbasaur.attack(chansey, latigoCepa);
		
		//debe sacar los 15 puntos de vida usual por el ataque
		assertEquals(115, chansey.getLife());
	}

	@Test
	public void test04AtaqueNormalSacaPuntosDeVidaSinMultiplicador(){
		AlgoMon jigglypuff 		= new Jigglypuff();
		AlgoMon chansey			= new Chansey();
		
		Ataque ataqueRapido 	= new AtaqueRapido();
		
		jigglypuff.attack(chansey, ataqueRapido);
		
		//debe sacar los 10 puntos de vida usual por el ataque
		assertEquals(120, chansey.getLife());
	}
	
	//ahora testeamos con cambio de estados aunque solo testeo el daño que le produce
	
	@Test
	public void test05AtaqueDeFuegoSacaPuntosDeVidaSinMultiplicador(){
		AlgoMon charmander 		= new Charmander();
		AlgoMon chansey			= new Chansey();
		
		Ataque fogonazo			= new Fogonazo();
		
		charmander.attack(chansey, fogonazo);
		
		//debe sacar los 2 puntos de vida usual por el ataque
		assertEquals(128, chansey.getLife());
		
	}
	
	//este ataque no posee cambio de estado
	@Test
	public void test06AtaqueDeAguaSacaPuntosDeVidaSinMultiplicador(){
		AlgoMon squirtle 		= new Squirtle();
		AlgoMon chansey			= new Chansey();
		
		Ataque canionDeAgua		= new CanionDeAgua();
		
		squirtle.attack(chansey, canionDeAgua);
		
		//debe sacar los 20 puntos de vida usual por el ataque
		assertEquals(110, chansey.getLife());
		
	}
	
	@Test
	public void test07AtaqueDePlantaSacaPuntosDeVidaSinMultiplicador(){
		AlgoMon bulbasaur		= new Bulbasaur();
		AlgoMon chansey			= new Chansey();
		
		Ataque chupavidas		= new Chupavidas(bulbasaur);
		
		bulbasaur.attack(chansey, chupavidas);
		
		//debe sacar los 15 puntos de vida usual por el ataque
		assertEquals(115, chansey.getLife());
	}

	@Test
	public void test08AtaqueNormalSacaPuntosDeVidaSinMultiplicador(){
		AlgoMon jigglypuff 		= new Jigglypuff();
		AlgoMon chansey			= new Chansey();
		
		Ataque canto		 	= new Canto();
		
		jigglypuff.attack(chansey, canto);
		
		//debe sacar los 0 puntos de vida usual por el ataque
		assertEquals(130, chansey.getLife());
	}
	
    @Test
    public void test09ChanseyEsAtacadoPorBurbujaYQuedaEn120DeVida(){
        AlgoMon chansey = new Chansey();
        Ataque burbuja = new Burbuja();

        chansey.attackedBy(burbuja);

        assertEquals(120, chansey.getLife());
    }

    @Test
    public void test10ChanseyEsAtacadoPorCanionDeAguaYQuedaEn110DeVida(){
        AlgoMon chansey = new Chansey();
        Ataque canion = new CanionDeAgua();

        chansey.attackedBy(canion);

        assertEquals(110, chansey.getLife());
    }

    @Test
    public void test11ChanseyEsAtacadoPorBrasasYQuedaEn114DeVida(){
        AlgoMon chansey = new Chansey();
        Ataque brasas = new Brasas();

        chansey.attackedBy(brasas);

        assertEquals(114, chansey.getLife());
    }

    @Test
    public void test12ChanseyEsAtacadoPorAtaqueRapidoYQuedaEn120DeVida(){
        AlgoMon chansey = new Chansey();
        Ataque ataqueRapido = new AtaqueRapido();

        chansey.attackedBy(ataqueRapido);

        assertEquals(120, chansey.getLife());
    }
    
    @Test
    public void test13ChanseySeQuemaYQuedaEn117DeVida(){
        AlgoMon chansey = new Chansey();
        chansey.quemarse();

        assertEquals(117, chansey.getLife());
    }
    
    @Test
    public void test14RattataSeQuemaYQuedaEn153DeVida(){
        AlgoMon rattata = new Rattata();
        rattata.quemarse();

        assertEquals(153, rattata.getLife());
    }
    
    @Test
    public void test15JigglypuffSeQuemaYQuedaEn117DeVida(){
        AlgoMon jigglypuff = new Jigglypuff();
        jigglypuff.quemarse();

        assertEquals(117, jigglypuff.getLife());
    }
}
