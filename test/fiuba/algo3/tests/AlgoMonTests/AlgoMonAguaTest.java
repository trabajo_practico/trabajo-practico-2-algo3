package fiuba.algo3.tests.AlgoMonTests;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.modelo.algomon.*;
import fiuba.algo3.modelo.ataque.*;


public class AlgoMonAguaTest {

	@Test
	public void test01AtaqueDeFuegoSacaPuntosDeVidaALaMitad(){
		AlgoMon charmander 		= new Charmander();
		AlgoMon squirtle 		= new Squirtle();
		
		Ataque brasas			= new Brasas();
		
		charmander.attack(squirtle, brasas);
		
		//debe sacar los 8 puntos de vida usual por el ataque
		assertEquals(142, squirtle.getLife());
		
	}
	
	@Test
	public void test02AtaqueDeAguaSacaPuntosDeVidaALaMitad(){
		AlgoMon squirtle 		= new Squirtle();
		AlgoMon otroSquirtle 	= new Squirtle();
		
		Ataque burbuja 			= new Burbuja();
		
		squirtle.attack(otroSquirtle, burbuja);
		
		//debe sacar los 5 puntos de vida usual por el ataque
		assertEquals(145, otroSquirtle.getLife());
		
	}
	
	@Test
	public void test03AtaqueDePlantaSacaPuntosDeVidaAlDoble(){
		AlgoMon bulbasaur		= new Bulbasaur();
		AlgoMon squirtle 		= new Squirtle();
		
		Ataque latigoCepa		= new LatigoCepa();
		
		bulbasaur.attack(squirtle, latigoCepa);
		
		//debe sacar los 15*2=30 puntos de vida usual por el ataque
		assertEquals(120, squirtle.getLife());
	}

	@Test
	public void test04AtaqueNormalSacaPuntosDeVidaSinMultiplicador(){
		AlgoMon jigglypuff 		= new Jigglypuff();
		AlgoMon squirtle 		= new Squirtle();
		
		Ataque ataqueRapido 	= new AtaqueRapido();
		
		jigglypuff.attack(squirtle, ataqueRapido);
		
		//debe sacar los 10 puntos de vida usual por el ataque
		assertEquals(140, squirtle.getLife());
	}
	
	//ahora testeamos con cambio de estados aunque solo testeo el daño que le produce
	
	@Test
	public void test05AtaqueDeFuegoSacaPuntosDeVidaALaMitad(){
		AlgoMon charmander 		= new Charmander();
		AlgoMon squirtle 		= new Squirtle();
		
		Ataque fogonazo			= new Fogonazo();
		
		charmander.attack(squirtle, fogonazo);
		
		//debe sacar los 2 puntos de vida usual por el ataque
		assertEquals(149, squirtle.getLife());
		
	}
	
	//este ataque no posee cambio de estado
	@Test
	public void test06AtaqueDeAguaSacaPuntosDeVidaALaMitad(){
		AlgoMon squirtle 		= new Squirtle();
		AlgoMon otroSquirtle 	= new Squirtle();
		
		Ataque canionDeAgua		= new CanionDeAgua();
		
		squirtle.attack(otroSquirtle, canionDeAgua);
		
		//debe sacar los 10 puntos de vida usual por el ataque
		assertEquals(140, otroSquirtle.getLife());
		
	}
	
	@Test
	public void test07AtaqueDePlantaSacaPuntosDeVidaAlDoble(){
		AlgoMon bulbasaur		= new Bulbasaur();
		AlgoMon squirtle 		= new Squirtle();
		
		Ataque chupavidas		= new Chupavidas(bulbasaur);
		
		bulbasaur.attack(squirtle, chupavidas);
		
		//debe sacar los 15*2 = 30 puntos de vida usual por el ataque
		assertEquals(120, squirtle.getLife());
	}

	@Test
	public void test08AtaqueNormalSacaPuntosDeVidaSinMultiplicador(){
		AlgoMon jigglypuff 		= new Jigglypuff();
		AlgoMon squirtle 		= new Squirtle();
		
		Ataque canto		 	= new Canto();
		
		jigglypuff.attack(squirtle, canto);
		
		//debe sacar los 0 puntos de vida usual por el ataque
		assertEquals(150, squirtle.getLife());
	}
	
	//testeo la recepcion de los ataques
	@Test
    public void test09SquirtleEsAtacadoPorLatigoCepaYQuedaEn120DeVida(){
        AlgoMon squirtle = new Squirtle();
        Ataque latigoCepa = new LatigoCepa();

        squirtle.attackedBy(latigoCepa);

        assertEquals(120, squirtle.getLife());
    }

    @Test
    public void test10SquirtleEsAtacadoPorBrasasYQuedaEn142DeVida(){
        AlgoMon squirtle = new Squirtle();
        Ataque brasas = new Brasas();

        squirtle.attackedBy(brasas);

        assertEquals(142, squirtle.getLife());
    }

    @Test
    public void test11SquirtleEsAtacadoPorBurbujaYQuedaEn145DeVida(){
        AlgoMon squirtle = new Squirtle();
        Ataque burbuja = new Burbuja();

        squirtle.attackedBy(burbuja);

        assertEquals(145, squirtle.getLife());
    }

    @Test
    public void test12SquirtleEsAtacadoPorAtaqueRapidoYQuedaEn140DeVida(){
        AlgoMon squirtle = new Squirtle();
        Ataque ataqueRapido = new AtaqueRapido();

        squirtle.attackedBy(ataqueRapido);

        assertEquals(140, squirtle.getLife());
    }
    
    @Test
    public void test13SquirtleSeQuemaYQuedaEn135DeVida(){
        AlgoMon squirtle = new Squirtle();
        squirtle.quemarse();

        assertEquals(135, squirtle.getLife());
    }

}
