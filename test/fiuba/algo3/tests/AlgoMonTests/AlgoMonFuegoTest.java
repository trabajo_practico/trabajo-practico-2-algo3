package fiuba.algo3.tests.AlgoMonTests;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.modelo.algomon.*;
import fiuba.algo3.modelo.ataque.*;
public class AlgoMonFuegoTest {

	@Test
	public void test01AtaqueDeFuegoSacaPuntosDeVidaALaMitad(){
		AlgoMon charmander 		= new Charmander();
		AlgoMon otroCharmander 	= new Charmander();
		
		Ataque brasas			= new Brasas();
		
		charmander.attack(otroCharmander, brasas);
		
		//debe sacar los 8 puntos de vida usual por el ataque
		assertEquals(162, otroCharmander.getLife());
		
	}
	
	@Test
	public void test02AtaqueDeAguaSacaPuntosDeVidaAlDoble(){
		AlgoMon squirtle 		= new Squirtle();
		AlgoMon charmander 		= new Charmander();
		
		Ataque burbuja 			= new Burbuja();
		
		squirtle.attack(charmander, burbuja);
		
		//debe sacar los 20 puntos de vida por el ataque
		assertEquals(150, charmander.getLife());
		
	}
	
	@Test
	public void test03AtaqueDePlantaSacaPuntosDeVidaALaMitad(){
		AlgoMon bulbasaur		= new Bulbasaur();
		AlgoMon charmander 		= new Charmander();
		
		Ataque latigoCepa		= new LatigoCepa();
		
		bulbasaur.attack(charmander, latigoCepa);
		
		//debe sacar los 7 puntos de vida por el ataque
		assertEquals(163, charmander.getLife());
	}

	@Test
	public void test04AtaqueNormalSacaPuntosDeVidaSinMultiplicador(){
		AlgoMon jigglypuff 		= new Jigglypuff();
		AlgoMon charmander 		= new Charmander();
		
		Ataque ataqueRapido 	= new AtaqueRapido();
		
		jigglypuff.attack(charmander, ataqueRapido);
		
		//debe sacar los 10 puntos de vida usual por el ataque
		assertEquals(160, charmander.getLife());
	}
	
	//ahora testeamos con cambio de estados aunque solo testeo el daño que le produce
	
	@Test
	public void test05AtaqueDeFuegoSacaPuntosDeVidaALaMitad(){
		AlgoMon charmander 			= new Charmander();
		AlgoMon otroCharmander 		= new Charmander();
		
		Ataque fogonazo				= new Fogonazo();
		
		charmander.attack(otroCharmander, fogonazo);
		
		//debe sacar los 1 punto de vida usual por el ataque
		assertEquals(169, otroCharmander.getLife());
		
	}
	
	//este ataque no posee cambio de estado
	@Test
	public void test06AtaqueDeAguaSacaPuntosDeVidaAlDoble(){
		AlgoMon squirtle 		= new Squirtle();
		AlgoMon charmander 		= new Charmander();
		
		Ataque canionDeAgua		= new CanionDeAgua();
		
		squirtle.attack(charmander, canionDeAgua);
		
		//debe sacar los 40 puntos de vida usual por el ataque
		assertEquals(130, charmander.getLife());
		
	}
	
	@Test
	public void test07AtaqueDePlantaSacaPuntosDeVidaALaMitad(){
		AlgoMon bulbasaur		= new Bulbasaur();
		AlgoMon charmander 		= new Charmander();
		
		Ataque chupavidas		= new Chupavidas(bulbasaur);
		
		bulbasaur.attack(charmander, chupavidas);
		
		//debe sacar los 15 puntos de vida usual por el ataque
		assertEquals(163, charmander.getLife());
	}

	@Test
	public void test08AtaqueNormalSacaPuntosDeVidaSinMultiplicador(){
		AlgoMon jigglypuff 		= new Jigglypuff();
		AlgoMon charmander 		= new Charmander();
		
		Ataque canto		 	= new Canto();
		
		jigglypuff.attack(charmander, canto);
		
		//debe sacar los 0 puntos de vida usual por el ataque
		assertEquals(170, charmander.getLife());
	}

	@Test
    public void test09CharmanderEsAtacadoPorBurbujaYQuedaEn150DeVida(){
        AlgoMon charmander = new Charmander();
        Ataque burbuja = new Burbuja();

        charmander.attackedBy(burbuja);

        assertEquals(150, charmander.getLife());
    }

    @Test
    public void test10CharmanderEsAtacadoPorAtaqueRapidoYQuedaEn160DeVida(){
        AlgoMon charmander = new Charmander();
        Ataque ataqueRapido = new AtaqueRapido();

        charmander.attackedBy(ataqueRapido);

        assertEquals(160, charmander.getLife());
    }

    @Test
    public void test11CharmanderEsAtacadoPorLatigoCepaYQuedaEn163DeVida(){
        AlgoMon charmander = new Charmander();
        Ataque latigoCepa = new LatigoCepa();

        charmander.attackedBy(latigoCepa);

        assertEquals(163, charmander.getLife());
    }

    @Test
    public void test12CharmanderEsAtacadoPorBrasasYQuedaEn162DeVida(){
        AlgoMon charmander = new Charmander();
        Ataque brasas = new Brasas();

        charmander.attackedBy(brasas);

        assertEquals(162, charmander.getLife());
    }
    
    @Test
    public void test13CharmanderSeQuemaYQuedaEn153DeVida(){
        AlgoMon charmander = new Charmander();
        charmander.quemarse();

        assertEquals(153, charmander.getLife());
    }

}
