package fiuba.algo3.tests.AlgoMonTests;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.modelo.algomon.*;
import fiuba.algo3.modelo.ataque.*;


public class AlgoMonPlantaTest {

	@Test
	public void test01AtaqueDeFuegoSacaPuntosDeVidaAlDoble(){
		AlgoMon charmander 		= new Charmander();
		AlgoMon bulbasaur		= new Bulbasaur();
		
		Ataque brasas			= new Brasas();
		
		charmander.attack(bulbasaur, brasas);
		
		//debe sacar los 16 * 2= 32 puntos de vida usual por el ataque
		assertEquals(108, bulbasaur.getLife());
		
	}
	
	@Test
	public void test02AtaqueDeAguaSacaPuntosDeVidaALaMitad(){
		AlgoMon squirtle 		= new Squirtle();
		AlgoMon bulbasaur		= new Bulbasaur();
		
		Ataque burbuja 			= new Burbuja();
		
		squirtle.attack(bulbasaur, burbuja);
		
		//debe sacar los 10/2 = 5 puntos de vida usual por el ataque
		assertEquals(135, bulbasaur.getLife());
		
	}
	
	@Test
	public void test03AtaqueDePlantaSacaPuntosDeVidaALaMitad(){
		AlgoMon bulbasaur		= new Bulbasaur();
		AlgoMon otroBulbasaur	= new Bulbasaur();
		
		Ataque latigoCepa		= new LatigoCepa();
		
		bulbasaur.attack(otroBulbasaur, latigoCepa);
		
		//debe sacar los 15/2 = 7 puntos de vida usual por el ataque
		assertEquals(133, otroBulbasaur.getLife());
	}

	@Test
	public void test04AtaqueNormalSacaPuntosDeVidaSinMultiplicador(){
		AlgoMon jigglypuff 		= new Jigglypuff();
		AlgoMon bulbasaur		= new Bulbasaur();
		
		Ataque ataqueRapido 	= new AtaqueRapido();
		
		jigglypuff.attack(bulbasaur, ataqueRapido);
		
		//debe sacar los 10 puntos de vida usual por el ataque
		assertEquals(130, bulbasaur.getLife());
	}
	
	//ahora testeamos con cambio de estados aunque solo testeo el daño que le produce
	
	@Test
	public void test05AtaqueDeFuegoSacaPuntosDeVidaAlDoble(){
		AlgoMon charmander 		= new Charmander();
		AlgoMon bulbasaur		= new Bulbasaur();
		
		Ataque fogonazo			= new Fogonazo();
		
		charmander.attack(bulbasaur, fogonazo);
		
		//debe sacar los 2 * 2 = 4 puntos de vida usual por el ataque
		assertEquals(136, bulbasaur.getLife());
		
	}
	
	//este ataque no posee cambio de estado
	@Test
	public void test06AtaqueDeAguaSacaPuntosDeVidaALaMitad(){
		AlgoMon squirtle 		= new Squirtle();
		AlgoMon bulbasaur		= new Bulbasaur();
		
		Ataque canionDeAgua		= new CanionDeAgua();
		
		squirtle.attack(bulbasaur, canionDeAgua);
		
		//debe sacar los 20 / 2 = 10 puntos de vida usual por el ataque
		assertEquals(130, bulbasaur.getLife());
		
	}
	
	@Test
	public void test07AtaqueDePlantaSacaPuntosDeVidaALaMitad(){
		AlgoMon bulbasaur		= new Bulbasaur();
		AlgoMon otroBulbasaur	= new Bulbasaur();
		
		Ataque chupavidas		= new Chupavidas(bulbasaur);
		
		bulbasaur.attack(otroBulbasaur, chupavidas);
		
		//debe sacar los 15 / 2 = 7 puntos de vida usual por el ataque
		assertEquals(133, otroBulbasaur.getLife());
	}

	@Test
	public void test08AtaqueNormalSacaPuntosDeVidaSinMultiplicador(){
		AlgoMon jigglypuff 		= new Jigglypuff();
		AlgoMon bulbasaur		= new Bulbasaur();
		
		Ataque canto		 	= new Canto();
		
		jigglypuff.attack(bulbasaur, canto);
		
		//debe sacar los 0 puntos de vida usual por el ataque
		assertEquals(140, bulbasaur.getLife());
	}
	
	//testeo la recepcion de los ataques
    @Test
    public void test09BulbasaurEsAtacadoPorAtaqueRapidYQuedaEn130DeVida(){
        AlgoMon bulbasaur = new Bulbasaur();
        Ataque ataqueRapido = new AtaqueRapido();

        bulbasaur.attackedBy(ataqueRapido);

        assertEquals(130, bulbasaur.getLife());
    }

    @Test
    public void test10BulbasaurEsAtacadoPorCanionDeAguaYQuedaEn130DeVida(){
        AlgoMon bulbasaur = new Bulbasaur();
        Ataque canion = new CanionDeAgua();

        bulbasaur.attackedBy(canion);

        assertEquals(130, bulbasaur.getLife());
    }

    @Test
    public void test11BulbasaurEsAtacadoPorFogonazoYQuedaEn136DeVida(){
        AlgoMon bulbasaur = new Bulbasaur();
        Ataque fogonazo = new Fogonazo();

        bulbasaur.attackedBy(fogonazo);

        assertEquals(136, bulbasaur.getLife());
    }

    @Test
    public void test12BulbasaurEsAtacadoPorBurbujaYQuedaEn135DeVida(){
        AlgoMon bulbasaur = new Bulbasaur();
        Ataque burbuja = new Burbuja();

        bulbasaur.attackedBy(burbuja);

        assertEquals(135, bulbasaur.getLife());
    }

    @Test
    public void test13BulbasaurSeQuemaYQuedaEn126DeVida(){
        AlgoMon bulbasaur = new Bulbasaur();
        bulbasaur.quemarse();

        assertEquals(126, bulbasaur.getLife());
    }
    
}
