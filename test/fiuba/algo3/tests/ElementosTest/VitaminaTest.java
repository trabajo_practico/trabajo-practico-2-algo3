package fiuba.algo3.tests.ElementosTest;

import fiuba.algo3.modelo.Elementos.Vitamina;
import fiuba.algo3.modelo.Partida.Partida;
import fiuba.algo3.modelo.algomon.*;
import fiuba.algo3.modelo.ataque.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class VitaminaTest {

    @Test
    public void test07ElJugador1AplicaUnaVitaminaASuAlgomonActualYAumenta2LaCantidadDeTodosLosAtaques(){
        Partida partida = new Partida();

        AlgoMon charmander = new Charmander();
        AlgoMon bulbasaur = new Bulbasaur();

        partida.agregarAlgomonAJugadorActual(charmander);
        partida.cambiarTurno();
        partida.agregarAlgomonAJugadorActual(bulbasaur);
        partida.cambiarTurno();

        List<Ataque> listaAtaques = new ArrayList<Ataque>();
        listaAtaques = partida.ataquesDeJugador();
        Brasas brasas = (Brasas) listaAtaques.get(0);
        Fogonazo fogonazo = (Fogonazo) listaAtaques.get(1);

        partida.jugadorAtacarCon(brasas);
        partida.jugadorAtacarCon(brasas);
        partida.jugadorAtacarCon(fogonazo);
        partida.jugadorAtacarCon(fogonazo);

        assertEquals(8, brasas.ataquesRestantes()); // Uso dos veces brasas
        assertEquals(2, fogonazo.ataquesRestantes()); // Uso dos veces fogonazo

        partida.jugadorAplicarElemento(new Vitamina());

        assertEquals(10, brasas.ataquesRestantes()); // Se restauraron 2 turnos de brasas
        assertEquals(4, fogonazo.ataquesRestantes()); // Se restauraron 2 turnos de fogonazo
    }

}
