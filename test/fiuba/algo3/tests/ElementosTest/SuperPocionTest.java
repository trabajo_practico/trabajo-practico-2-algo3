package fiuba.algo3.tests.ElementosTest;


import fiuba.algo3.modelo.Elementos.SuperPocion;
import fiuba.algo3.modelo.Partida.Partida;
import fiuba.algo3.modelo.algomon.*;
import fiuba.algo3.modelo.ataque.Brasas;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SuperPocionTest {

    @Test
    public void test01ElJugador2AplicaUnaSuperPocionASuAlgomonActualDañadoYLeRegenera40PuntosDeVida(){
        Partida partida = new Partida();

        AlgoMon charmander = new Charmander();
        AlgoMon bulbasaur = new Bulbasaur();

        partida.agregarAlgomonAJugadorActual(charmander);
        partida.cambiarTurno();
        partida.agregarAlgomonAJugadorActual(bulbasaur);
        partida.cambiarTurno();

        partida.jugadorAtacarCon(new Brasas());
        partida.jugadorAtacarCon(new Brasas());
        partida.cambiarTurno();

        partida.jugadorAplicarElemento(new SuperPocion());

        assertEquals(116, bulbasaur.getLife());
    }

}
