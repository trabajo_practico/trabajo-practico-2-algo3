package fiuba.algo3.tests.ElementosTest;

import fiuba.algo3.modelo.Elementos.Pocion;
import fiuba.algo3.modelo.Partida.Partida;
import fiuba.algo3.modelo.algomon.*;
import fiuba.algo3.modelo.ataque.Brasas;
import org.junit.Test;
import static org.junit.Assert.*;

public class PocionTest {

    @Test
    public void test01ElJugador2AplicaUnaPocionASuAlgomonActualDañadoYLeRegenera20PuntosDeVida(){
        Partida partida = new Partida();

        AlgoMon charmander = new Charmander();
        AlgoMon bulbasaur = new Bulbasaur();

        partida.agregarAlgomonAJugadorActual(charmander);
        partida.cambiarTurno();
        partida.agregarAlgomonAJugadorActual(bulbasaur);
        partida.cambiarTurno();

        partida.jugadorAtacarCon(new Brasas());
        partida.cambiarTurno();

        partida.jugadorAplicarElemento(new Pocion());

        assertEquals(128, bulbasaur.getLife());
    }

}
