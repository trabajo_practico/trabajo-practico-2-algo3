package fiuba.algo3.tests.ElementosTest;


import fiuba.algo3.modelo.Elementos.Restaurador;
import fiuba.algo3.modelo.Partida.Partida;
import fiuba.algo3.modelo.algomon.*;
import fiuba.algo3.modelo.ataque.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RestauradorTest {

    @Test
    public void test06ElJugador2AplicaUnRestauradorASuAlgomonActualQuemadoYVuelveAEstadoNormal(){
        Partida partida = new Partida();

        AlgoMon charmander = new Charmander();
        AlgoMon bulbasaur = new Bulbasaur();

        partida.agregarAlgomonAJugadorActual(charmander);
        partida.cambiarTurno();
        partida.agregarAlgomonAJugadorActual(bulbasaur);
        partida.cambiarTurno();

        partida.jugadorAtacarCon(new Fogonazo()); //-4 vida a bulbasaur (136 de vida)
        partida.cambiarTurno();

        bulbasaur.attack(charmander, new LatigoCepa()); // bulbasaur se quema por 14 (122 de vida)
        partida.jugadorAplicarElemento(new Restaurador());

        bulbasaur.attack(charmander, new LatigoCepa()); // bulbasaur no se quema (122 de vida)

        assertEquals(122, bulbasaur.getLife());
    }

}
