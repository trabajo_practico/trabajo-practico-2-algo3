package fiuba.algo3.tests;

import fiuba.algo3.modelo.Partida.Partida;
import fiuba.algo3.modelo.algomon.*;
import fiuba.algo3.modelo.ataque.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class PartidaTest {

    @Test
    public void test01SeIniciaUnaPartidaCon2JugadoresYCadaUnoPuedeElegir3Algomones(){
        Partida partida = new Partida();

        partida.agregarAlgomonAJugadorActual(new Bulbasaur());
        partida.cambiarTurno();
        partida.agregarAlgomonAJugadorActual(new Bulbasaur());
        partida.cambiarTurno();
        partida.agregarAlgomonAJugadorActual(new Charmander());
        partida.cambiarTurno();
        partida.agregarAlgomonAJugadorActual(new Charmander());
        partida.cambiarTurno();
        partida.agregarAlgomonAJugadorActual(new Squirtle());
        partida.cambiarTurno();
        partida.agregarAlgomonAJugadorActual(new Squirtle());
        partida.cambiarTurno();

        assertEquals(3, partida.cantidadAlgomonesRestantes());
        partida.cambiarTurno();
        assertEquals(3, partida.cantidadAlgomonesRestantes());
    }

    @Test
    public void test02Jugador1AtacaConCharmanderConBrasasAlBulbasaurDelJugador2(){
        Partida partida = new Partida();

        AlgoMon charmander = new Charmander();
        AlgoMon bulbasaur = new Bulbasaur();

        partida.agregarAlgomonAJugadorActual(charmander);
        partida.cambiarTurno();
        partida.agregarAlgomonAJugadorActual(bulbasaur);
        partida.cambiarTurno();

        partida.jugadorAtacarCon(new Brasas());

        assertEquals(108, bulbasaur.getLife());
    }

    @Test
    public void test03ElJugador1CambiaDeSuAlgomonActualDeCharmanderABulbasaur(){
        Partida partida = new Partida();

        AlgoMon charmander = new Charmander();
        AlgoMon bulbasaur = new Bulbasaur();

        partida.agregarAlgomonAJugadorActual(charmander);
        partida.cambiarTurno();
        partida.agregarAlgomonAJugadorActual(charmander);
        partida.cambiarTurno();

        partida.cambiarAlgomonActualA(bulbasaur);

        assertEquals(Bulbasaur.class, partida.getAlgomonActual().getClass());
    }

}
